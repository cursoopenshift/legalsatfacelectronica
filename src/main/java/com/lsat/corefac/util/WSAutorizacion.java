package com.lsat.corefac.util;


import java.util.ArrayList;
import java.util.List;

import com.lsat.corefac.model.util.Autorizacion;
import com.lsat.corefac.model.util.MensajeAutorizacion;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class WSAutorizacion
        extends SolicitarWebService {

    private String XML_AUTORIZACION = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://ec.gob.sri.ws.autorizacion\"><env:Header/><env:Body><ns1:autorizacionComprobante><claveAccesoComprobante>#1</claveAccesoComprobante></ns1:autorizacionComprobante></env:Body></env:Envelope>";
    private String XML;
    private Autorizacion autorizacionOk;
    private Autorizacion autorizacionKo;
    private List<Autorizacion> listaAutorizacion;

    public WSAutorizacion() {
        setAutorizacionOk(new Autorizacion());
        setAutorizacionKo(new Autorizacion());
    }

    public void procesar(String direccion, String clave) {
        int i = 0;
        while (i < 7) {
            try {
                setXML(ejecutarConsultaWebService(this.XML_AUTORIZACION.replaceAll("#1", clave), direccion));

                i++;
                return;
            } catch (Exception e) {
                setMensaje("No se pudo instanciar el servicio al SRI autorizacion ." + direccion + " clave " + clave + " " + e.toString());
                if ((getEstadoConsulta() == 500) || (getEstadoConsulta() == 502) || (getEstadoConsulta() == 503) || (e.toString().toUpperCase().contains("Connection reset".toUpperCase()))) {
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void descuartizaXML() {
        setListaAutorizacion(new ArrayList());
        Document document = parseXmlFile(getXML());
        NodeList nodeLst = document.getElementsByTagName("autorizacion");
        for (int i = 0; i < nodeLst.getLength(); i++) {
            Node nodo = nodeLst.item(i);
            Autorizacion autorizacion = new Autorizacion();
            for (int k = 0; k < nodo.getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("estado")) {
                    autorizacion.setEstado(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("ambiente")) {
                    autorizacion.setAmbiente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("comprobante")) {
                    autorizacion.setComprobante(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("fechaAutorizacion")) {
                    autorizacion.setFechaAutorizacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("numeroAutorizacion")) {
                    autorizacion.setNumeroAutorizacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("mensajes")) {
                    NodeList nodeLstMensajes = nodo.getChildNodes().item(k).getChildNodes();
                    for (int j = 0; j < nodeLstMensajes.getLength(); j++) {
                        Node nodoMensaje = nodeLstMensajes.item(j);

                        MensajeAutorizacion mensajeAutorizacion = new MensajeAutorizacion();
                        for (int l = 0; l < nodoMensaje.getChildNodes().getLength(); l++) {
                            if (nodoMensaje.getChildNodes().item(l).getNodeName().equalsIgnoreCase("identificador")) {
                                mensajeAutorizacion.setIdentificador(nodoMensaje.getChildNodes().item(l).getTextContent());
                            }
                            if (nodoMensaje.getChildNodes().item(l).getNodeName().equalsIgnoreCase("mensaje")) {
                                mensajeAutorizacion.setMensaje(nodoMensaje.getChildNodes().item(l).getTextContent());
                            }
                            if (nodoMensaje.getChildNodes().item(l).getNodeName().equalsIgnoreCase("tipo")) {
                                mensajeAutorizacion.setTipo(nodoMensaje.getChildNodes().item(l).getTextContent());
                            }
                            if (nodoMensaje.getChildNodes().item(l).getNodeName().equalsIgnoreCase("informacionAdicional")) {
                                mensajeAutorizacion.setInformacionAdicional(nodoMensaje.getChildNodes().item(l).getTextContent());
                            }
                        }
                        autorizacion.getListaMensajeAutorizacion().add(mensajeAutorizacion);
                    }
                }
            }
            if (autorizacion.getEstado().equalsIgnoreCase("AUTORIZADO")) {
                setAutorizacionOk(autorizacion);
            } else if (getAutorizacionKo().getFecha().before(autorizacion.getFecha())) {
                setAutorizacionKo(autorizacion);
            }
            getListaAutorizacion().add(autorizacion);
        }
    }

    public String toString() {
        String autorizaciones = "";
        for (Autorizacion a : getListaAutorizacion()) {
            autorizaciones = autorizaciones + a.toString();
        }
        return "\nautorizaciones: " + autorizaciones + toString(true);
    }

    public String toString(boolean j) {
        return "\nAutorizacion: " + getAutorizacionOk().toString() + "\nNo Autorizado: " + getAutorizacionKo().toString() + "\nxml: " + getXML();
    }

    public Autorizacion getAutorizacionOk() {
        return this.autorizacionOk;
    }

    public void setAutorizacionOk(Autorizacion autorizacionOk) {
        this.autorizacionOk = autorizacionOk;
    }

    public Autorizacion getAutorizacionKo() {
        return this.autorizacionKo;
    }

    public void setAutorizacionKo(Autorizacion autorizacionKo) {
        this.autorizacionKo = autorizacionKo;
    }

    public String getXML() {
        return this.XML;
    }

    public void setXML(String XML) {
        this.XML = XML;
    }

    public List<Autorizacion> getListaAutorizacion() {
        return this.listaAutorizacion;
    }

    public void setListaAutorizacion(List<Autorizacion> listaAutorizacion) {
        this.listaAutorizacion = listaAutorizacion;
    }
}
