package com.lsat.corefac.util;

public class UtilEnum {

    public enum PASO {
        REGISTRO, FIRMADO,ARMADO_XML_AUTORIZACION, GENERACION_PDF, ENVIO_MAIL, RECEPCION, AUTORIZACION, FINALIZACION
    }


    public enum EstadoDocumento {
        REGISTRADO("R"),RECIBIDA("S"), AUTORIZADO("A"),NOAUTORIZADO("N");
        private String code;

        private EstadoDocumento(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    public enum TipoEmisionEnum {
        NORMAL("NORMAL"), CONTINGENCIA("INDISPONIBILIDAD DE SISTEMA");

        private String code;

        private TipoEmisionEnum(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    public enum TipoAmbienteEnum {
        PRODUCCION("2"), PRUEBAS("1");
        private String code;

        private TipoAmbienteEnum(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }


}
