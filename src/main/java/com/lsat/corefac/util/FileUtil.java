package com.lsat.corefac.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
@Slf4j
@UtilityClass
public class FileUtil {



    public static String getText(String path) {
        try {
            File fichaEntrada = new File(path);
            FileInputStream canalEntrada = new FileInputStream(fichaEntrada);
            byte[] bt = new byte[(int) fichaEntrada.length()];
            canalEntrada.read(bt);
            canalEntrada.close();
            return new String(bt);
        } catch (IOException e) {
            log.error(String.format("Error read Archivo: %s", e));
        }
        return null;
    }

    public static boolean setText(String path,String text) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
            osw.write(text);
            osw.flush();
            osw.close();
            fos.close();
            return true;
        } catch (Exception ex) {
            log.error(String.format("Error Crear Archivo: %s", ex));
            return false;
        }
    }

}
