package com.lsat.corefac.util;

import lombok.experimental.UtilityClass;

import java.util.Map;

@UtilityClass
public class StringUtils {


    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    public static String generatePathLocate(String homePath, String ruc) {

        return  homePath+UtilConstants.FILE_SEPARATOR+ruc.trim();
    }

    public static String generateNameFile(String pathCompany,String nombreDoc, String fechaEmision, String numDoc, String claveAcceso) {
        return pathCompany+UtilConstants.FILE_SEPARATOR+nombreDoc +UtilConstants.FILE_SEPARATOR+fechaEmision+UtilConstants.FILE_SEPARATOR+(numDoc+"_"+claveAcceso+".xml");
    }

    public static String remplace(String value, Map<String,String> map) {
        if(map==null || value==null){
            return  value;
        }
        String result="";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            value=value.replaceAll(entry.getKey(),entry.getValue());
        }
        return value;
    }


}
