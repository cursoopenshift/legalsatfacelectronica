package com.lsat.corefac.util;

import lombok.experimental.UtilityClass;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@UtilityClass
public class FolderUtils {
    private static final String FILE_SEPARATOR = File.separator;
    private static final String FILE_PROTOCOL_NAME = "file:";

    public static boolean checkIfExistFile(String filePathString) {
        if (!Files.exists(Paths.get(filePathString))) {
            return false;
        }
        return true;
    }

    public static boolean createFolderIFNOExistFile(String filePathString) {
        try {
            Files.createDirectories(Paths.get(filePathString));
        } catch (Exception e) {
        }
        return true;
    }

    public static String readFileAsString(String fileName) throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }

    public static String buildFolderPath(List<String> directories, Optional<String> arguments) {
        StringBuilder sb = new StringBuilder(FILE_PROTOCOL_NAME);
        for (String directory : directories) {
            sb.append(directory);
            if (!sb.toString().endsWith(FILE_SEPARATOR)) {
                sb.append(FILE_SEPARATOR);
            }
        }
        if (arguments.isPresent()) {
            sb.append("?").append(arguments.get());
        }
        return sb.toString();
    }

    public static boolean isDirectory(String folderPath) {
        File rootFolder = new File(folderPath);
        return rootFolder.exists() && rootFolder.isDirectory();
    }
}
