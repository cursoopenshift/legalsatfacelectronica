package com.lsat.corefac.util;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * Clase para hacer una solicitud de web services.
 */
public class SolicitarWebService {

    private static String MSJ_ERROR_EXCEPTION = "Error %s con el mensaje del servidor: %s";
    private String mensaje;
    private int timeOut = 30000;
    private int estadoConsulta = 0;
    public static final String TEXTO_VERIFICACION_FACTURA = "<factura id=";
    public static final String INICIO_TAG_COMPROBANTE = "<comprobante><![CDATA[";
    public static final String FIN_TAG_COMPROBANTE = "]]></comprobante>";
    public static final String INICIO_TAG_COMPROBANTE_SIN_CDATA = "<comprobante>";
    public static final String FIN_TAG_COMPROBANTE_SIN_CDATA = "</comprobante>";

    public SolicitarWebService() {
    }

    public String ejecutarConsultaWebService(String xmlRequest, String direccion)
            throws Exception {
        tratamientSSL();
        if (getMensaje() != null) {
            return null;
        }
        String repuestaSOAP = "";
        String responseString = "";
        InputStream inputStream = null;
        URL url = new URL(direccion);
        URLConnection connection = url.openConnection();
        connection.setDoOutput(true);
        connection.setConnectTimeout(this.timeOut);
        connection.setReadTimeout(this.timeOut);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream(), Charset.forName("UTF-8"));
        outputStreamWriter.write(xmlRequest);
        outputStreamWriter.close();

        setEstadoConsulta(((HttpURLConnection) connection).getResponseCode());
        if (getEstadoConsulta() != 200) {
            inputStream = ((HttpURLConnection) connection).getErrorStream();
        } else {
            inputStream = connection.getInputStream();
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while ((responseString = bufferedReader.readLine()) != null) {
            repuestaSOAP = repuestaSOAP + responseString;
        }
        bufferedReader.close();
        if (getEstadoConsulta() != 200) {
            throw new Exception(String.format(MSJ_ERROR_EXCEPTION, new Object[]{Integer.valueOf(getEstadoConsulta()), repuestaSOAP}));
        }
        repuestaSOAP = corregir(repuestaSOAP);
        return repuestaSOAP;
    }

    private String corregir(String xml) {
        if (xml.contains("&lt;")) {
            xml = xml.replace("&lt;", "<");
        }
        if (xml.contains("&gt;")) {
            xml = xml.replace("&gt;", ">");
        }
        if (xml.contains(INICIO_TAG_COMPROBANTE_SIN_CDATA)) {
            xml = xml.replace(INICIO_TAG_COMPROBANTE_SIN_CDATA, INICIO_TAG_COMPROBANTE);
        }
        if (xml.contains(FIN_TAG_COMPROBANTE_SIN_CDATA)) {
            xml = xml.replace(FIN_TAG_COMPROBANTE_SIN_CDATA, FIN_TAG_COMPROBANTE);
        }
        return xml;
    }

    public void tratamientSSL() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            setMensaje(this.getClass().getName() + ".tratamientSSL() " + e.toString());
        }
    }
    public String XMLText(String direccion) {
        // MainFiles archivoLog = new MainFiles(direccion);
        // return archivoLog.getText_nuevo();
        return "";
    }

    public String textXML(String direccion, String texto) {
        setMensaje(null);
        texto = "<?xml version='1.0' encoding='UTF-8' ?> " + texto;
        if(!FileUtil.setText(direccion, texto)){
            setMensaje("Error al Crear el archivo en la path: "+direccion);
        }
        return "ok";
    }

    public String formatXML(String unformattedXml) {
        try {
            return unformattedXml;
        } catch (Exception e) {
            setMensaje(getClass().getName() + ".formatXML() " + e.toString());
        }
        return null;
    }

    public Document parseXmlFile(String in) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(in));
            return db.parse(is);
        } catch (Exception e) {
            setMensaje(getClass().getName() + ".parseXmlFile() " + e.toString());
        }
        return null;
    }

    public String getMensaje() {
        return this.mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getEstadoConsulta() {
        return this.estadoConsulta;
    }

    public void setEstadoConsulta(int estadoConsulta) {
        this.estadoConsulta = estadoConsulta;
    }

}