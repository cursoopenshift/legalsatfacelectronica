package com.lsat.corefac.util;

import java.nio.charset.StandardCharsets;

import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

@Getter
@Setter
public class Recepcion
        extends SolicitarWebService {

    private String XML_RECEPCION = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://ec.gob.sri.ws.recepcion\"> <env:Header/><env:Body><ns1:validarComprobante><xml>#1</xml></ns1:validarComprobante></env:Body></env:Envelope>";

    private String error;

    private String informacionAdicional;

    private String identificador;

    private String estado;

    private String claveAcceso;
    private String XML;

    public Recepcion() {
        setError("");
        setClaveAcceso("");
        setEstado("");
        setIdentificador("");
        setInformacionAdicional("");
        setMensaje(null);
        setXML("");
    }
    public void procesar(String direccion, String archivoFirmado) {
        buscaClave(archivoFirmado);
        if (getMensaje() != null) {
            return;
        }
        String decodedBytes = new String(java.util.Base64.getMimeEncoder().encode(archivoFirmado.getBytes()),
                StandardCharsets.UTF_8);
        int i = 0;
        while (i < 7) {
            try {
                setXML(ejecutarConsultaWebService(this.XML_RECEPCION.replaceAll("#1", decodedBytes), direccion));
                i++;
                return;
            } catch (Exception e) {
                setMensaje("No se pudo instanciar el servicio al SRI autorizacion ." + direccion + " archivo " + archivoFirmado + " " + e.toString());
                if ((getEstadoConsulta() == 500) || (getEstadoConsulta() == 502) || (getEstadoConsulta() == 503) || (e.toString().toUpperCase().contains("Connection reset".toUpperCase()))) {
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    private String corregirXml(String xml) {
        if (xml.contains(INICIO_TAG_COMPROBANTE)) {
            xml = xml.replace(INICIO_TAG_COMPROBANTE, "");
        }
        if (xml.contains(FIN_TAG_COMPROBANTE)) {
            xml = xml.replace(FIN_TAG_COMPROBANTE, "");
        }
        return xml;
    }

    public void descuartizaXML() {
        Document document = parseXmlFile(corregirXml(getXML()));

        //Document document = parseXmlFile(getXML());
        NodeList nodeLst = document.getElementsByTagName("estado");
        try {
            setEstado(nodeLst.item(0).getTextContent());
        } catch (Exception e) {
        }
        try {
            nodeLst = document.getElementsByTagName("tipo");
            setError(nodeLst.item(0).getTextContent());
        } catch (Exception e) {
        }
        try {
            nodeLst = document.getElementsByTagName("informacionAdicional");
            setInformacionAdicional(nodeLst.item(0).getTextContent());
        } catch (Exception e) {
        }
        try {
            nodeLst = document.getElementsByTagName("mensajes");
            nodeLst = nodeLst.item(0).getChildNodes();
            setMensaje(nodeLst.item(1).getTextContent().split("\n")[1].trim());
        } catch (Exception e) {
        }
        try {
            nodeLst = document.getElementsByTagName("identificador");
            setIdentificador(nodeLst.item(0).getTextContent());
        } catch (Exception e) {
        }
    }

    public void buscaClave(String archivoFirmado) {
        try {
            Document document = parseXmlFile(archivoFirmado);
            NodeList nodeLst = document.getElementsByTagName("claveAcceso");
            setClaveAcceso(nodeLst.item(0).getTextContent());
        } catch (Exception e) {
            setMensaje(getClass().getName() + ".buscaClave() " + e.toString());
        }
    }
}




