package com.lsat.corefac.util;

import java.io.File;

public class UtilConstants {

    public static final String DETELE_DOCUMENTO_XML = "</#document>";
    public static final String HOME_FOLDER = "ProcesoFacturacion";
    public static final String FILE_SEPARATOR = File.separator;

    public static final  String HEADER_XML_AUTORIZACION = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><autorizacion><estado>AUTORIZADO</estado>"
            + "<numeroAutorizacion>_CLAVE_AUTORIZACION_</numeroAutorizacion>"
            + "<fechaAutorizacion>_FECHA_AUTORIZACION_</fechaAutorizacion>"
            + "<comprobante><![CDATA[";
    public static final String FOOT_XML_AUTORIZACION = "]]></comprobante></autorizacion>";


}
