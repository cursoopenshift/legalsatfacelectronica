package com.lsat.corefac.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class IdentificarDocumento {

    private int codigoTipoDocumento;
    private String nombreTipoDocumento;
    private String version;
    private String identicacion;
    private String tipoIdentificacion;
    private String secuencia;
    private String claveAcesso;
    private String correo;
    private String identificacionCliente;
    private String direccionCliente;
    private String correoCliente;
    private String telefonoCliente;
    private String nombreCliente;
    private String nombreEmpresa;
    private String valor;
    private String ambiente;
    private boolean XMLValido = true;
    private List<String> identidadesGuia;
    private Autorizacion autorizacionOk;
    private Document documento;

    public IdentificarDocumento() {
    }

    public IdentificarDocumento(String direccion) {
        setIdenticacion("");
        setIdentificacionCliente("");
        setClaveAcesso("");
        setCodigoTipoDocumento(0);
        setCorreo("");
        setCorreoCliente("");
        setDireccionCliente("");
        setNombreCliente("");
        setSecuencia("");
        setTelefonoCliente("");
        setTipoIdentificacion("");
        setValor("");
        setNombreTipoDocumento("");
        setNombreEmpresa("");
        setAmbiente("");
        setAutorizacionOk(new Autorizacion());
        setIdentidadesGuia(new ArrayList());
        this.documento = parseXmlFile(direccion);
    }

    private Document parseXmlFile1(String direccion) {
        this.XMLValido = false;
        try {
            File fXmlFile = new File(direccion);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document documentoTemporal = dBuilder.parse(fXmlFile);

            this.XMLValido = true;
            return documentoTemporal;
        } catch (Exception e) {
            Logger.getLogger("global").log(Level.SEVERE, e.toString());
            this.XMLValido = false;
        }
        return null;
    }

    private Document parseXmlFile(String xml) {
        this.XMLValido = false;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            this.XMLValido = true;
            return builder.parse(is);
        } catch (Exception e) {
            Logger.getLogger("global").log(Level.SEVERE, e.toString());
            this.XMLValido = false;
        }
        return null;
    }

    private Document parseXmlString(String texto) {
        texto = texto.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("\"", "'").replaceAll("UTF-8", "ISO-8859-1").replaceAll("utf-8", "ISO-8859-1");

        this.XMLValido = false;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(texto.trim().getBytes());

            Document documentoTemporal = dBuilder.parse(stream);

            this.XMLValido = true;
            return documentoTemporal;
        } catch (IOException | ParserConfigurationException | SAXException e) {
            Logger.getLogger("global").log(Level.SEVERE, e.toString());
            this.XMLValido = false;
        }
        return null;
    }

    public String descuartizaXMLOriginal() {
        System.out.println("entro a decuartizar");
        if (!isXMLValido()) {
            return "XML invalido";
        }
        String tipoDocumento = this.documento.getDocumentElement().getNodeName();
        String version = this.documento.getDocumentElement().getAttributeNode("version").getValue();
        setVersion(version);
        getInfoTributaria();
        if (tipoDocumento.equalsIgnoreCase("factura")) {
            informacionComprador("infoFactura");
            valorDocumento("infoFactura", "importeTotal");
            setCodigoTipoDocumento(1);
            setNombreTipoDocumento("Factura");
        }
        if (tipoDocumento.equalsIgnoreCase("notaDebito")) {
            informacionComprador("infoNotaDebito");
            valorDocumento("infoNotaDebito", "valorTotal");
            setCodigoTipoDocumento(5);
            setNombreTipoDocumento("nota debito");
        }
        if (tipoDocumento.equalsIgnoreCase("notaCredito")) {
            informacionComprador("infoNotaCredito");
            valorDocumento("infoNotaCredito", "valorModificacion");
            setCodigoTipoDocumento(4);
            setNombreTipoDocumento("Nota Credito");
        }

        if (tipoDocumento.equalsIgnoreCase("comprobanteRetencion")) {
            infoCompRetencion();
            // valorDocumento("impuesto", "valorRetenido");
            setCodigoTipoDocumento(7);
            setNombreTipoDocumento("comprobante retencion");
        }
        if (tipoDocumento.equalsIgnoreCase("guiaRemision")) {
            infoGuiaRemision();
            setCodigoTipoDocumento(6);
            setNombreTipoDocumento("guia remision");
        }

        if (tipoDocumento.equalsIgnoreCase("liquidacionCompra")) {
            informacionProveedor("infoLiquidacionCompra");
            valorDocumento("infoLiquidacionCompra", "importeTotal");
            setCodigoTipoDocumento(3);
            setNombreTipoDocumento("Liquidacion Compra");
        }

        infoAdicional();

        return null;
    }

    private void valorDocumento(String informacionDocumento, String nodoValor) {
        double valor = 0.0D;
        DecimalFormat df = new DecimalFormat("#.00");

        NodeList nodeList = this.documento.getElementsByTagName(informacionDocumento);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase(nodoValor)) {
                    try {
                        valor = Double.parseDouble(nodo.getChildNodes().item(k).getTextContent()) + valor;
                    } catch (NumberFormatException | DOMException e) {
                        valor = 0.0D;
                    }
                }
            }
        }
        String valorAux = df.format(valor);
        setValor(valorAux != null ? valorAux.replaceAll(",", ".") : valorAux);
    }

    public String descuartizaXMLAutorizado() {
        NodeList nodeLst = this.documento.getElementsByTagName("autorizacion");
        for (int i = 0; i < nodeLst.getLength(); i++) {
            Node nodo = nodeLst.item(i);
            Autorizacion autorizacion = new Autorizacion();
            for (int k = 0; k < nodo.getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("ambiente")) {
                    autorizacion.setAmbiente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("fechaAutorizacion")) {
                    autorizacion.setFechaAutorizacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("numeroAutorizacion")) {
                    autorizacion.setNumeroAutorizacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("estado")) {
                    autorizacion.setEstado(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("comprobante")) {
                    autorizacion.setComprobante(nodo.getChildNodes().item(k).getTextContent());
                }
                if ((autorizacion.getEstado().equalsIgnoreCase("AUTORIZADO")) && (autorizacion.getComprobante().trim().length() > 0)) {
                    this.documento = parseXmlString(autorizacion.getComprobante());
                    descuartizaXMLOriginal();
                    setAutorizacionOk(autorizacion);
                    return null;
                }
            }
        }
        return null;
    }

    private void getInfoTributaria() {
        NodeList nodeList = this.documento.getElementsByTagName("infoTributaria");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("razonSocial")) {
                    setNombreEmpresa(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("ambiente")) {
                    setAmbiente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("ruc")) {
                    setIdenticacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("claveAcceso")) {
                    setClaveAcesso(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("estab")) {
                    setSecuencia(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("ptoEmi")) {
                    setSecuencia(getSecuencia() + "-" + nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("secuencial")) {
                    setSecuencia(getSecuencia() + "-" + nodo.getChildNodes().item(k).getTextContent());
                }
            }
        }
    }

    private void infoGuiaRemision() {
        NodeList nodeList = this.documento.getElementsByTagName("infoGuiaRemision");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("rucTransportista")) {
                    setIdentificacionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("tipoIdentificacionTransportista")) {
                    setTipoIdentificacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("dirEstablecimiento")) {
                    setDireccionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("razonSocialTransportista")) {
                    setNombreCliente(nodo.getChildNodes().item(k).getTextContent());
                }
            }
        }
        nodeList = this.documento.getElementsByTagName("destinatario");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("identificacionDestinatario")) {
                    addLista(nodo.getChildNodes().item(k).getTextContent());
                }
            }
        }
    }

    private void infoCompRetencion() {
        NodeList nodeList = this.documento.getElementsByTagName("infoCompRetencion");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("identificacionSujetoRetenido")) {
                    setIdentificacionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("tipoIdentificacionSujetoRetenido")) {
                    setIdentificacionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("dirEstablecimiento")) {
                    setDireccionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("razonSocialSujetoRetenido")) {
                    setNombreCliente(nodo.getChildNodes().item(k).getTextContent());
                }
            }
        }
    }

    private void informacionComprador(String tipoInformacion) {
        NodeList nodeList = this.documento.getElementsByTagName(tipoInformacion);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("identificacionComprador")) {
                    setIdentificacionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("tipoIdentificacionComprador")) {
                    setTipoIdentificacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("dirEstablecimiento")) {
                    setDireccionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("razonSocialComprador")) {
                    setNombreCliente(nodo.getChildNodes().item(k).getTextContent());
                }
            }
        }
    }

    private void informacionProveedor(String tipoInformacion) {
        NodeList nodeList = this.documento.getElementsByTagName(tipoInformacion);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("identificacionProveedor")) {
                    setIdentificacionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("tipoIdentificacionProveedor")) {
                    setTipoIdentificacion(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("dirEstablecimiento")) {
                    setDireccionCliente(nodo.getChildNodes().item(k).getTextContent());
                }
                if (nodo.getChildNodes().item(k).getNodeName().equalsIgnoreCase("razonSocialProveedor")) {
                    setNombreCliente(nodo.getChildNodes().item(k).getTextContent());
                }
            }
        }
    }

    private void infoAdicional() {
        NodeList nodeList = this.documento.getElementsByTagName("infoAdicional");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodo = nodeList.item(i);
            for (int k = 0; k < nodeList.item(i).getChildNodes().getLength(); k++) {
                if (nodo.getChildNodes().item(k).hasAttributes()) {
                    Node nodoHijo = nodo.getChildNodes().item(k).getAttributes().getNamedItem("nombre");
                    if ((nodoHijo.getNodeValue().equalsIgnoreCase("Tel�fono Cliente")) || (nodoHijo.getNodeValue().equalsIgnoreCase("Telefono Cliente"))) {
                        setTelefonoCliente(nodo.getChildNodes().item(k).getTextContent());
                    }
                    if (nodoHijo.getNodeValue().equalsIgnoreCase("Correo Cliente")) {
                        setCorreoCliente(nodo.getChildNodes().item(k).getTextContent());
                    }
                    if (nodoHijo.getNodeValue().contains("Correo")) {
                        setCorreo(getCorreo() + " " + nodo.getChildNodes().item(k).getTextContent());
                    }
                }
            }
        }
    }

    private void addLista(String identificacion) {
        for (String a : getIdentidadesGuia()) {
            if (a.compareTo(identificacion) == 0) {
                return;
            }
        }
        getIdentidadesGuia().add(identificacion);
    }

    public String toString() {
        String listadoGuias = "";
        for (String a : getIdentidadesGuia()) {
            listadoGuias = a + "\n" + listadoGuias;
        }
        return String.format("Identificacion: %s, Codigo Tipo Documento: %s, Secuenca: %s, Clave de acceso: %s, Nombre Cliente: %s, Identificaci�n Cliente : %s, Correos: %s,Direccion Cliente: %s, Tipo Identificacion: %s, Correo Cliente: %s,Telefono cliente: %s,Valor: %s\n%s", new Object[]{getIdenticacion(), Integer.valueOf(getCodigoTipoDocumento()), getSecuencia(), getClaveAcesso(), getNombreCliente(), getIdentificacionCliente(), getCorreo(), getDireccionCliente(), getTipoIdentificacion(), getCorreoCliente(), getTelefonoCliente(), getValor(), listadoGuias}) + this.autorizacionOk.toString();
    }

    public String getSecuencia() {
        return this.secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getClaveAcesso() {
        return this.claveAcesso;
    }

    public void setClaveAcesso(String claveAcesso) {
        this.claveAcesso = claveAcesso;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIdenticacion() {
        return this.identicacion;
    }

    public void setIdenticacion(String identicacion) {
        this.identicacion = identicacion;
    }

    public int getCodigoTipoDocumento() {
        return this.codigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(int codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public String getIdentificacionCliente() {
        return this.identificacionCliente;
    }

    public void setIdentificacionCliente(String identificacionCliente) {
        this.identificacionCliente = identificacionCliente;
    }

    public String getCorreoCliente() {
        return this.correoCliente;
    }

    public void setCorreoCliente(String correoCliente) {
        this.correoCliente = correoCliente;
    }

    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getDireccionCliente() {
        return this.direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    public String getTelefonoCliente() {
        return this.telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public String getNombreCliente() {
        return this.nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public List<String> getIdentidadesGuia() {
        return this.identidadesGuia;
    }

    public void setIdentidadesGuia(ArrayList<String> identidadesGuia) {
        this.identidadesGuia = identidadesGuia;
    }

    public String getValor() {
        if (this.valor == null || this.valor.isEmpty()) {
            return "0";
        }
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void setXMLValido(boolean XMLValido) {
        this.XMLValido = XMLValido;
    }

    public boolean isXMLValido() {
        return this.XMLValido;
    }

    public Autorizacion getAutorizacionOk() {
        return this.autorizacionOk;
    }

    public void setAutorizacionOk(Autorizacion autorizacionOk) {
        this.autorizacionOk = autorizacionOk;
    }

    public String getNombreTipoDocumento() {
        return this.nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public String getNombreEmpresa() {
        return this.nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     * @return the ambiente
     */
    public String getAmbiente() {
        return ambiente;
    }

    /**
     * @param ambiente the ambiente to set
     */
    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public class Autorizacion {

        private String estado;
        private String fechaAutorizacion;
        private Date fecha;
        private String ambiente;
        private String comprobante;
        private String numeroAutorizacion;
        private List<IdentificarDocumento.MensajeAutorizacion> listaMensajeAutorizacion;

        public Autorizacion() {
            setAmbiente("");
            setComprobante("");
            setEstado("");
            setFechaAutorizacion("2000-07-15T18:00:05.752-05:00");
            setListaMensajeAutorizacion(new ArrayList());
            setNumeroAutorizacion("");
        }

        public String toString() {
            return String.format("Ambiente %s, comprobante %s, estado %s, no autorizacion %s, fecha autorizacion %s, mensajes: %s ", new Object[]{getAmbiente(), getComprobante(), getEstado(), getNumeroAutorizacion(), getFechaAutorizacion(), listaMensajesToString()});
        }

        public String getEstado() {
            return this.estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getFechaAutorizacion() {
            return this.fechaAutorizacion;
        }

        public void setFechaAutorizacion(String fechaAutorizacion) {
            this.fechaAutorizacion = fechaAutorizacion;
        }

        public String getAmbiente() {
            return this.ambiente;
        }

        public void setAmbiente(String ambiente) {
            this.ambiente = ambiente;
        }

        public String getComprobante() {
            return this.comprobante;
        }

        public void setComprobante(String comprobante) {
            this.comprobante = comprobante;
        }

        public List<IdentificarDocumento.MensajeAutorizacion> getListaMensajeAutorizacion() {
            return this.listaMensajeAutorizacion;
        }

        public void setListaMensajeAutorizacion(List<IdentificarDocumento.MensajeAutorizacion> listaMensajeAutorizacion) {
            this.listaMensajeAutorizacion = listaMensajeAutorizacion;
        }

        public String listaMensajesToString() {
            String respuesta = "";
            for (IdentificarDocumento.MensajeAutorizacion a : getListaMensajeAutorizacion()) {
                respuesta = respuesta + a.toString();
            }
            return respuesta;
        }

        public Date getFecha() {
            try {
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                String strFecha = getFechaAutorizacion().replaceAll("T", " ").substring(1, 19);

                return formato.parse(strFecha);
            } catch (Exception ex) {
            }
            return new Date();
        }

        public void setFecha(Date fecha) {
            this.fecha = fecha;
        }

        public String getNumeroAutorizacion() {
            return this.numeroAutorizacion;
        }

        public void setNumeroAutorizacion(String numeroAutorizacion) {
            this.numeroAutorizacion = numeroAutorizacion;
        }
    }

    public class MensajeAutorizacion {

        private String identificador;
        private String mensaje;
        private String tipo;
        private String informacionAdicional;

        public MensajeAutorizacion() {
            setIdentificador("");
            setMensaje("");
            setTipo("");
            setInformacionAdicional("");
        }

        public String toString() {
            return "\nidentificador: " + getIdentificador() + "\nmensaje: " + getMensaje() + "\ntipo: " + getTipo();
        }

        public String getIdentificador() {
            return this.identificador;
        }

        public void setIdentificador(String identificador) {
            this.identificador = identificador;
        }

        public String getMensaje() {
            return this.mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }

        public String getTipo() {
            return this.tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public String getInformacionAdicional() {
            return this.informacionAdicional;
        }

        public void setInformacionAdicional(String informacionAdicional) {
            this.informacionAdicional = informacionAdicional;
        }
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }
}
