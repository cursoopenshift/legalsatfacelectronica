package com.lsat.corefac.util.exception;

public class InvalidCertificateException extends RuntimeException {

    public InvalidCertificateException(String cause) {
        super(cause);
    }
}