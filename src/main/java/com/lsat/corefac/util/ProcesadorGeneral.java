package com.lsat.corefac.util;

import com.lsat.corefac.model.*;
import com.lsat.corefac.model.util.Autorizacion;
import com.lsat.corefac.model.util.MensajeAutorizacion;
import com.lsat.corefac.model.util.ResponseGeneric;
import com.lsat.corefac.repository.DocumentRepository;
import com.lsat.corefac.repository.ParameterRepository;
import com.lsat.corefac.repository.StepRepository;
import com.lsat.corefac.repository.XmlRepository;
import com.lsat.corefac.util.email.EmailUtil;
import com.lsat.corefac.util.email.EnvioMailAdjuntos;
import com.lsat.corefac.util.firma.Signer;
import com.lsat.corefac.util.pdf.GenerarFactura;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

@Slf4j
@Service
public class ProcesadorGeneral {
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private XmlRepository xmlRepository;
    @Autowired
    private StepRepository stepRepository;
    @Autowired
    private ParameterRepository parameterRepository;

    private Map<String, Parameter> parameterHasp;

    private Document document;
    private Xml xml;
    private Company company;

    @Value("${url.sri.recepcion.dev}")
    String urlRecepcionDev;

    @Value("${url.sri.recepcion.pro}")
    String urlRecepcionPro;

    @Value("${url.sri.autorizacion.dev}")
    String urlAutorizacionDev;

    @Value("${url.sri.autorizacion.pro}")
    String urlAutorizacionPro;
    @Value("${sleep.sri.pro}")
    int sleepSriPro;

    public void execute1(Company company, Document document, Xml xml) throws ExecutionException, InterruptedException {

        this.document = document;
        this.xml = xml;
        this.company = company;
        this.parameterHasp = new HashMap<>();

        CompletableFuture<ResponseGeneric> future = CompletableFuture.supplyAsync(new Supplier<ResponseGeneric>() {
            @Override
            public ResponseGeneric get() {
                try {
                    if (!initParameter()) {


                    }

                    if (!firmarDocumento()) {

                    }
                    if (!generarXmlAutorizacion()) {

                    }
                    return new ResponseGeneric(false, "", null);
                } catch (Exception e) {

                    return null;
                }
            }
        });

        ResponseGeneric result = future.get();
    }


    public void execute(Company company, Document document, Xml xml) {

        this.document = document;
        this.xml = xml;
        this.company = company;
        this.parameterHasp = new HashMap<>();
        if (!initParameter()) {
            return;
        }
        if (!firmarDocumento()) {
            return;
        }
        if (!generarXmlAutorizacion()) {
            return;
        }

        if (!generarRide()) {
            return;
        }

        if (!enviarMail()) {
            return;
        }

        if (!recepcion()) {
            return;
        }

        if (!sleep()) {
            return;
        }
        if (!autorizacion()) {
            return;
        }
        return;
    }

    private boolean initParameter() {
        List<Parameter> parameters = this.parameterRepository.findByCcompany(this.document.getCcompany());
        if (parameters == null || parameters.isEmpty()) {
            return false;
        }
        for (Parameter param : parameters) {
            this.parameterHasp.put(param.getCode().trim().toUpperCase(), param);
        }
        //crea el directorio
        FolderUtils.createFolderIFNOExistFile(this.document.getPathLocal());
        FolderUtils.createFolderIFNOExistFile( Paths.get(this.document.getPathRide()).getParent().toString());


        return true;
    }

    private boolean firmarDocumento() {
        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.FIRMADO.name(), new Date().toString()));
        try {
            String pathFirma = this.parameterHasp.get("FIRMAELECTRONICA").getValor1();
            if (!FolderUtils.checkIfExistFile(pathFirma)) {
                registrarError(UtilEnum.PASO.FIRMADO, "Firma NOK", "No existe la firma en la ruta:" + pathFirma);
                return false;
            }
            String passFirma = this.parameterHasp.get("FIRMAELECTRONICA").getValor2();
            File tempXml = File.createTempFile(this.document.getKeyAcces(), ".xml");
            Files.writeString(tempXml.toPath(), this.xml.getXmlNormal(), StandardCharsets.UTF_8, StandardOpenOption.APPEND);
            Signer signer = new Signer();
            File tempXmlSing = File.createTempFile(this.document.getKeyAcces() + "_Firmada_", ".xml");
            signer.sign(pathFirma, passFirma, tempXml.getPath(), tempXmlSing.getPath());
            String xmlSing = FolderUtils.readFileAsString(tempXmlSing.getPath());
            String originalString = xmlSing;
            if (xmlSing.indexOf(UtilConstants.DETELE_DOCUMENTO_XML) > 0) {
                xmlSing = xmlSing.replaceAll(UtilConstants.DETELE_DOCUMENTO_XML, "");
                byte[] byteText = xmlSing.getBytes(Charset.forName("UTF-8"));
                try {
                    originalString = new String(byteText, "UTF-8");
                } catch (UnsupportedEncodingException ex) {
                    registrarError(UtilEnum.PASO.FIRMADO, "Firma NOK", "Error al firmar:" + ex.toString());
                }
            }
            this.xml.setXmlSigned(originalString);
            this.xml = this.xmlRepository.save(this.xml);
            registrarPaso(UtilEnum.PASO.FIRMADO, "Firma OK","");
            return true;
        } catch (Exception e) {
            registrarError(UtilEnum.PASO.FIRMADO, "Firma NOK", "Error general al firmar :" + e.toString());
            return false;
        }

    }

    private boolean generarXmlAutorizacion() {
        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.ARMADO_XML_AUTORIZACION.name(), new Date().toString()));
        try {
            String xmlAutorizado = "";
            String fechaAutorizacion = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            IdentificarDocumento identificarDocumento = new IdentificarDocumento(this.xml.getXmlNormal());
            identificarDocumento.descuartizaXMLOriginal();
            this.document.setAmbiente(identificarDocumento.getAmbiente());
            this.document.setDateSRI(fechaAutorizacion);
            this.document.setKeyAcces(identificarDocumento.getClaveAcesso());
            this.document.setValue(Double.parseDouble(identificarDocumento.getValor()));
            this.document.setState(UtilEnum.EstadoDocumento.NOAUTORIZADO.getCode());
            this.documentRepository.save(this.document);
            Map<String, String> map = new HashMap<String, String>();
            {
                map.put("_CLAVE_AUTORIZACION_", identificarDocumento.getClaveAcesso());
                map.put("_FECHA_AUTORIZACION_", fechaAutorizacion);
            }
            xmlAutorizado = StringUtils.remplace(UtilConstants.HEADER_XML_AUTORIZACION, map) + xml.getXmlSigned() + UtilConstants.FOOT_XML_AUTORIZACION;
            this.xml.setXmlAuthorized(xmlAutorizado);
            this.xml = this.xmlRepository.save(this.xml);
            return true;
        } catch (Exception ex) {
            log.error(String.format("Paso :%s, Error: %s", UtilEnum.PASO.ARMADO_XML_AUTORIZACION.name(), ex));
            return false;
        }
    }

    private boolean recepcion() {

        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.RECEPCION.name(), new Date().toString()));
        try {
            String archivoFirmando = this.xml.getXmlSigned();
            Recepcion recepcion = new Recepcion();
            String urlRecepcion = this.document.getAmbiente().equalsIgnoreCase("1") ? this.urlRecepcionDev : this.urlRecepcionPro;
            recepcion.procesar(urlRecepcion, archivoFirmando);
            if (recepcion.getMensaje() != null) {
                registrarPaso(UtilEnum.PASO.RECEPCION, "Error al invocar ws recepcion", recepcion.getMensaje());
                return false;
            }
            recepcion.descuartizaXML();
            if (recepcion.getEstado().compareToIgnoreCase("DEVUELTA") == 0) {
                registrarPaso(UtilEnum.PASO.RECEPCION, "El web service devolvio estado devuelto", recepcion.getEstado() + " " + recepcion.getError() + " " + recepcion.getIdentificador() + " " + recepcion.getInformacionAdicional());
                return false;
            }
            registrarPaso(UtilEnum.PASO.RECEPCION, "Recepcion OK", "");
            this.document.setState(UtilEnum.EstadoDocumento.RECIBIDA.getCode());
            this.document.setDateRecepcion(new Date());
            this.documentRepository.save(this.document);
            return true;
        } catch (Exception ex) {
            log.error(String.format("Paso :%s, Error: %s", UtilEnum.PASO.RECEPCION.name(), ex));
            registrarPaso(UtilEnum.PASO.RECEPCION, "Recepcion NOK", ex.toString());
            return false;
        }
    }

    private boolean sleep() {
        try {
            Thread.sleep(sleepSriPro);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    private boolean generarRide() {
        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.GENERACION_PDF.name(), new Date().toString()));
        boolean error = false;
        String mensaje = "";
        if (this.document.getAliasDoc().equalsIgnoreCase("01")) { // factura
            GenerarFactura generarFactura = new GenerarFactura();
            generarFactura.setFormatoDetallColSizeSubsidio(null);
            generarFactura.setFormatoDetallColSizeSinSubsidio(null);
            generarFactura.setDireccionLogotipo(this.document.getPathLocal()+UtilConstants.FILE_SEPARATOR+this.document.getRuc()+".png");
            generarFactura.setAmbiente(this.document.getAmbiente());
            generarFactura.setClaveAcceso(this.document.getKeyAcces());
            generarFactura.setEmision(this.document.getEmision());
            generarFactura.setFechaAutorizacion(this.document.getDateSRI());
            generarFactura.setNumeroAutorizacion(this.document.getKeyAcces());
            try {
                generarFactura.generarPDF(this.xml.getXmlNormal(), this.document.getPathRide());
            } catch (Exception ex) {
                log.error(String.format("Paso :%s, Error: %s", UtilEnum.PASO.GENERACION_PDF.name(), ex));
                mensaje = ex.toString();
                error = true;
            }
        } // fin genera factura

        if (this.document.getAliasDoc().equalsIgnoreCase("07")) { // comprobante retencion
            //// TODO: 28/2/2023

        }
        if (!error) {
            registrarPaso(UtilEnum.PASO.GENERACION_PDF, "Generacion PDF OK", "");
            this.document.setGeneraPdf(true);
            this.documentRepository.save(this.document);
            return true;
        } else {
            registrarPaso(UtilEnum.PASO.GENERACION_PDF, "Error al Generar PDF", mensaje);
            return false;
        }
    }

    private boolean enviarMail() {
        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.ENVIO_MAIL.name(), new Date().toString()));
        boolean error = false;
        String mensaje = "";


        if (this.document.getEmails() == null || this.document.getEmails().isEmpty()) {
            registrarPaso(UtilEnum.PASO.ENVIO_MAIL, "Error al enviar email", "no hay correos");
            return true;
        }

        List<String> listaAdjuntos = new ArrayList();
        List<String> correos = new ArrayList();
        String[] correosAux = this.document.getEmails().split(",");
        correos.addAll(Arrays.asList(correosAux));

        EnvioMailAdjuntos envioMailAdjuntos = new EnvioMailAdjuntos();
        EmailUtil emailUtil = new EmailUtil();
        emailUtil.setListaAdjuntos(listaAdjuntos);
        emailUtil.setListEmails(correos);

        try {
            envioMailAdjuntos.enviarCorreoAdjuntosDocumentosElectronicos(emailUtil);
        } catch (Exception e) {
            registrarPaso(UtilEnum.PASO.ENVIO_MAIL, "Error al enviar email", e.toString());
            return true;
        }

        return true;
    }


    private boolean autorizacion() {
        log.info(String.format("Paso :%s  Fecha: %s", UtilEnum.PASO.AUTORIZACION.name(), new Date().toString()));
        try {
            WSAutorizacion wsAutorizacion = new WSAutorizacion();
            String urlAutorizacion = this.document.getAmbiente().equalsIgnoreCase("1") ? this.urlAutorizacionDev : this.urlAutorizacionPro;
            wsAutorizacion.procesar(urlAutorizacion, this.document.getKeyAcces());
            if (wsAutorizacion.getMensaje() != null) {
                registrarError(UtilEnum.PASO.AUTORIZACION, "Autorizacion NOK", wsAutorizacion.getMensaje());
                return false;
            }
            wsAutorizacion.descuartizaXML();
            wsAutorizacion.textXML(this.document.getPathAutorizado(), wsAutorizacion.getXML());
            if (wsAutorizacion.getMensaje() != null) {
                registrarError(UtilEnum.PASO.AUTORIZACION, "Autorizacion NOK", wsAutorizacion.getMensaje());
                return false;
            }
            if (wsAutorizacion.getAutorizacionOk().getEstado().compareToIgnoreCase("") == 0) {
                this.document.setAmbiente(wsAutorizacion.getAutorizacionKo().getAmbiente());
                this.document.setEstadoAutorizacion(wsAutorizacion.getAutorizacionKo().getEstado());
                if (wsAutorizacion.getListaAutorizacion() != null && !wsAutorizacion.getListaAutorizacion().isEmpty()) {
                    for (MensajeAutorizacion a : ((Autorizacion) wsAutorizacion.getListaAutorizacion().get(0)).getListaMensajeAutorizacion()) {
                        String descripcionError = "Tipo: " + a.getTipo() + ", Identificador: " + a.getIdentificador() + ", Mensaje: " + a.getMensaje() + ", Infoprmacion Adicional:" + a.getInformacionAdicional();
                        registrarError(UtilEnum.PASO.AUTORIZACION, "Autorizacion NOK", descripcionError);
                    }
                }
                registrarError(UtilEnum.PASO.AUTORIZACION, "Autorizacion NOK", "NO SE PUDO AUTORIZAR");
                documentRepository.save(this.document);
                return false;
            }
            this.document.setDateSRI(wsAutorizacion.getAutorizacionOk().getFechaAutorizacion());
            this.document.setAmbiente(wsAutorizacion.getAutorizacionOk().getAmbiente());
            this.document.setNumAutorizacion(wsAutorizacion.getAutorizacionOk().getNumeroAutorizacion());
            this.document.setEstadoAutorizacion(wsAutorizacion.getAutorizacionOk().getEstado());
            this.document.setAmbiente(wsAutorizacion.getAutorizacionOk().getAmbiente());
            this.document.setDateFinalize(new Date());
            this.document.setState(UtilEnum.EstadoDocumento.AUTORIZADO.getCode());
            this.documentRepository.save(this.document);
            registrarPaso(UtilEnum.PASO.AUTORIZACION, "Autorizacion OK", "AUTORIZACION PROCESADO CON EXISTO");
            return true;
        } catch (Exception ex) {
            log.error(String.format("Paso :%s, Error: %s", UtilEnum.PASO.AUTORIZACION.name(), ex));
            registrarPaso(UtilEnum.PASO.AUTORIZACION, "Autorizacion NOK", ex.toString());
            return false;
        }
    }

    private void registrarPaso(UtilEnum.PASO paso, String mensaje, String descripcion) {
        try {
            Step step = new Step();
            step.setCdocument(this.document.getCdocument());
            step.setNumStep(paso.name());
            step.setMesager(mensaje);
            step.setError(false);
            step.setDescription(descripcion);
            this.stepRepository.save(step);
            log.info(String.format("Paso :%s, Error: %s", paso.name(), descripcion));
        } catch (Exception ex) {
            log.error(String.format("Error: %s", ex));
        }
    }

    private void registrarError(UtilEnum.PASO paso, String mensaje, String descripcion) {
        try {
            Step step = new Step();
            step.setCdocument(this.document.getCdocument());
            step.setNumStep(paso.name());
            step.setMesager(mensaje);
            step.setError(true);
            step.setDescription(descripcion);
            this.stepRepository.save(step);
            log.error(String.format("Paso :%s, Error: %s", paso.name(), descripcion));
        } catch (Exception ex) {
            log.error(String.format("Error: %s", ex));
        }
    }
}
