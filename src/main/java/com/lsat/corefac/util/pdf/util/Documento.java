package com.lsat.corefac.util.pdf.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;
@Slf4j
public class Documento extends Document {

    private PdfWriter writer;
    private PdfContentByte contentbyte;

    /**Metodo para crear el objeto.
     *
     * @param fileName
     */
    public Documento(String fileName) {
        super(PageSize.A4, 36, 36, 36, 36);
        try {
            writer = PdfWriter.getInstance(this, new FileOutputStream(fileName));
        } catch (Exception e) {
            log.error("Error al inicializar elemento: "+e.toString());
        }
    }

    /**Metodo que retorna el objeto write.
     * @return
     */
    public PdfWriter getWriter() {
        return writer;
    }

    /**Metodo permite ingrese el write del documento pdf.
     * @param writer
     */
    public void setWriter(PdfWriter writer) {
        this.writer = writer;
    }

    /**Metodo que retorna el contentbay de un documento.
     * @return contentbyte
     */
    public PdfContentByte getContentbyte() {
        contentbyte = getWriter().getDirectContent();
        return contentbyte;
    }

    /**
     * @param contentbyte
     */
    public void setContentbyte(PdfContentByte contentbyte) {
        this.contentbyte = contentbyte;
    }



}
