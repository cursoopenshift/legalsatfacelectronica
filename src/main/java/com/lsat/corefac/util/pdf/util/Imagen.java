package com.lsat.corefac.util.pdf.util;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import lombok.extern.slf4j.Slf4j;

import java.awt.Color;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
@Slf4j
public class Imagen extends Elemento {
    private int _x;
    private int _y;

    private String path;
    private URL url;
    private float scala;
    private com.itextpdf.text.Image imagen;

    public Imagen() {
        setScala(100.0F);
    }

    public void escribe() {
        try {
            getImagen().scalePercent(getScala());
            getImagen().setAbsolutePosition(getX(), getY());
            setScala(getScala());
            setX(getX());
            setY(getY());
            setPath(getPath());
            this.url = null;
        } catch (Exception e) {
            log.error("Error al : "+e.toString());
        }
    }



    public String getPath() {
        return this.path;
    }

    public void setPath(String URL) {
        this.path = URL;
    }

    public float getScala() {
        return this.scala;
    }

    public void setScala(float scala) {
        this.scala = scala;
    }

    public com.itextpdf.text.Image getImagen() {
        if ((getPath() == null) && (getUrl() == null)) {
            return this.imagen;
        }
        if (getPath() != null) {
            try {
                if (this.imagen == null) {
                    this.imagen = com.itextpdf.text.Image.getInstance(getPath());

                }
            } catch (Exception e) {
                log.error("Error al : "+e.toString());

                return null;
            }
        }
        if (getUrl() != null) {
            try {
                if (this.imagen == null) {
                    this.imagen = com.itextpdf.text.Image.getInstance(getUrl());

                }
            } catch (Exception e) {
                log.error("Error al : "+e.toString());

                return null;
            }
        }
        return this.imagen;
    }

    public void setImagen(com.itextpdf.text.Image imagen) {
        this.imagen = imagen;
    }

    public void setImagen(java.awt.Image i) {
        try {
            this.imagen = com.itextpdf.text.Image.getInstance(i, Color.WHITE);
        } catch (Exception e) {
        }
    }

    public URL getUrl() {
        return this.url;
    }

    public void setUrl(String stringUrl) {

        try {
            this.url = new URL(stringUrl);
        } catch (MalformedURLException ex) {
            log.error("Error al : "+ex.toString());
        }

    }

    public void setUrl(URL url) {
        this.url = url;
    }

    /**
     * @return the _x
     */
    public int getX() {
        return _x;
    }

    /**
     * @param _x the _x to set
     */
    public void setX(int _x) {
        this._x = _x;
    }

    /**
     * @return the _y
     */
    public int getY() {
        return _y;
    }

    /**
     * @param _y the _y to set
     */
    public void setY(int _y) {
        this._y = _y;
    }



}
