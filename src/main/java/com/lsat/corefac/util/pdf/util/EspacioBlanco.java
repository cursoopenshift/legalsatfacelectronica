package com.lsat.corefac.util.pdf.util;

import com.itextpdf.text.Font;

public class EspacioBlanco extends Elemento{
    public EspacioBlanco() {
        super();
        limpiar();
        setFont(new Font(getBf(), 6, Font.NORMAL));
    }

    /**Metodo que retorna un String.
     * @return
     */
    public String getTexto() {
        return " ";
    }


}
