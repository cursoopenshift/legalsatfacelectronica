package com.lsat.corefac.util.pdf;

import com.itextpdf.text.pdf.PdfWriter;
import com.lsat.corefac.model.util.docs.FE.Factura;
import com.lsat.corefac.model.util.docs.FE.Pagos;
import com.lsat.corefac.util.UtilTabla;
import com.lsat.corefac.util.pdf.util.*;
import lombok.Getter;
import lombok.Setter;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.itextpdf.text.DocumentException;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;

@Getter
@Setter
@Slf4j
public class GenerarFactura {

    private static String[] TOTALES = {"SUBTOTAL 12%:", "SUBTOTAL 0%:", "SUBTOTAL No Obj IVA:", "SUBTOTAL:", "DESCUENTO:", "ICE:", "IVA 12%:", "PROPINA:", "FLETE INTERNACIONAL:", "SEGURO INTERNACIONAL:", "GASTOS ADUANEROS:", "OTROS GASTOS DE TRANSPORTE:", "VALOR TOTAL:"};
    private static String LLAVE_DIRECCION_CLIENTE = "direccion";
    private static String LLAVE_TELEFONO_CLIENTE = "telefono,tel?fono";
    private static String LLAVE_CORREO_CLIENTE = "email,e-mail,correo,correos";
    private String numeroAutorizacion = "8901234567891123456789212345678931234567894";
    private String fechaAutorizacion = "01/08/2014 21:11:01";
    private String ambiente = "Pendiente";
    private String emision = "Normal";
    private String claveAcceso = "12345678901234567890123456789012345678901234567890";
    private String direccionLogotipo;
    private Factura bus = null;
    private List<TotalFactura> totales;
    private List<Pagos.Pago> pagos;
    private Encabezado encabezado;
    private LineaNormal lineaNormal;
    private LineaCentrada lineaCentrada;
    private TablaHorizontal tablaHorizontal;
    private TablaVertical tablaVertical;
    private Titulo1 titulo1;
    private Titulo2 titulo2;
    private Titulo3 titulo3;
    private Titulo4 titulo4;
    private EspacioBlanco espacioBlanco;
    private Documento documento;
    private Cabezera cabezera;
    private String formatoDetallColSizeSubsidio = null;
    private String formatoDetallColSizeSinSubsidio = null;


    public GenerarFactura() {
        encabezado = new Encabezado();
        espacioBlanco = new EspacioBlanco();
        lineaNormal = new LineaNormal();
        lineaCentrada = new LineaCentrada();
        tablaHorizontal = new TablaHorizontal();
        tablaVertical = new TablaVertical();
        titulo1 = new Titulo1();
        titulo2 = new Titulo2();
        titulo3 = new Titulo3();
        titulo4 = new Titulo4();
        cabezera = new Cabezera();
        setTotales(new ArrayList<TotalFactura>());
        setDireccionLogotipo("");
        inicializaTotales();
    }

    public void generarPDF(String fileNameXml, String fileNamePdf) throws JAXBException, FileNotFoundException, DocumentException {
        String instancePath = "com.lsat.corefac.model.util.docs.FE";
        JAXBContext jc = JAXBContext.newInstance(instancePath);
        Unmarshaller u = jc.createUnmarshaller();
        StringReader reader = new StringReader(fileNameXml);
        setBus((Factura) u.unmarshal(reader));

        cabezera.setBus(getBus());
        cabezera.setDireccionLogotipo(getDireccionLogotipo());
        cabezera.setEmision(getEmision());
        cabezera.setClaveAcceso(getClaveAcceso());
        cabezera.setFechaAutorizacion(getFechaAutorizacion());
        cabezera.setNumeroAutorizacion(getNumeroAutorizacion());
        cabezera.setAmbiente(getAmbiente());

        documento = new Documento(fileNamePdf);
        PdfWriter writer = documento.getWriter();
        cabezera.setWriter(writer);
        writer.setPageEvent(cabezera);
        writer.setPageEvent(encabezado);
        documento.setMargins(40, 30, 30, 40);
        informacionPiePaguina();
        documento.open();

        try {
            informaicionCliente();
        } catch (Exception e) {
            log.error(" informacion Cliente mal", e);
        }
        try {
            detalleDocumento();
        } catch (Exception e) {
            log.error(" informacion detalleDocumento mal", e);
        }
        try {
            totalDetalleDocumento();
            informacionExportacion();
        } catch (Exception e) {
            log.error(" total detalle documento mal", e);
        }
        try {
            formaPago();
        } catch (Exception e) {
            log.error(" forma de pago mal", e);
        }
        documento.close();
    }


    private void formaPago() throws DocumentException {
        espacionBlanco(1);
        if (getBus() != null && getBus().getInfoFactura() != null && getBus().getInfoFactura() != null
                && getBus().getInfoFactura().getPagos() != null) {
            pagos = getBus().getInfoFactura().getPagos().getPago();
            if (pagos != null) {
                espacionBlanco(1);
                titulo2.setTexto("INFORMACIÓN FORMA PAGO");
                titulo2.CambiarPosicion((float) 9.5);
                documento.add(titulo2.getElemento());
                lineaCentrada.cambiarTamana(74);
                documento.add(lineaCentrada.getLinea());
                for (Pagos.Pago item_pago : pagos) {
                    Object contenido[] = new Object[]{UtilTabla.ObtenerTabla("formaPago", item_pago.getFormaPago()), item_pago.getPlazo() + " "
                            + item_pago.getUnidadTiempo(), "$" + item_pago.getTotal()};
                    tablaVertical.limpiar();
                    tablaVertical.setTitulos(new String[]{"Forma Pago:", "Plazo:", "Total:"});
                    tablaVertical.setContenidos(contenido);
                    tablaVertical.setAlineamientos(new int[]{0, 0});
                    tablaVertical.llenarTabla(false);
                    tablaVertical.setTamanos(new int[]{30, 70});
                    tablaVertical.setPosicion(2);
                    tablaVertical.setAnchoTabla(100);
                    documento.add(tablaVertical.getTabla());
                }
            }
        }
        espacionBlanco(1);
        if (getBus().getInfoAdicional() != null && getBus().getInfoAdicional().getCampoAdicional().size() > 0) {
            String titulos[] = new String[getBus().getInfoAdicional().getCampoAdicional().size()];
            Object contenido[] = new Object[getBus().getInfoAdicional().getCampoAdicional().size()];
            int i = 0;
            for (Factura.InfoAdicional.CampoAdicional a : getBus().getInfoAdicional().getCampoAdicional()) {
                if (datosCliente(LLAVE_CORREO_CLIENTE, a.getNombre())) {
                    if (datosCliente(LLAVE_DIRECCION_CLIENTE, a.getNombre())) {
                        if (datosCliente(LLAVE_TELEFONO_CLIENTE, a.getNombre())) {
                            titulos[i] = a.getNombre() + ":";
                            contenido[i] = a.getValue();
                        }
                    }

                }
                i++;
            }
            titulo2.setTexto("INFORMACIÓN ADICIONAL");
            documento.add(titulo2.getElemento());
            titulo2.CambiarPosicion((float) 9.5);
            lineaCentrada.cambiarTamana(76);
            documento.add(lineaCentrada.getLinea());
            tablaVertical.limpiar();
            tablaVertical.setTitulos(titulos);
            tablaVertical.setContenidos(contenido);
            tablaVertical.setAlineamientos(new int[]{0, 0});
            tablaVertical.llenarTabla(false);
            tablaVertical.setTamanos(new int[]{30, 60});
            tablaVertical.setPosicion(2);
            tablaVertical.setAnchoTabla(100);
            documento.add(tablaVertical.getTabla());
        }
    }



    /**
     * Metodo para calcular el cambio de pagina.
     *
     * @param tamano
     * @return
     */
    /**
     * Metodo que permite ingresar detalles de la factura.
     *
     * @throws DocumentException
     */
    private void totalDetalleDocumento() throws DocumentException {
        tablaVertical.limpiar();

        int[] alineamientos = {0, 2};
        int[] tamanos = {35, 17};
        Vector datos = new Vector();
        Vector<String> titulos = new Vector<String>();
        cargarTotales(getBus());

        if (getTotales() != null && getTotales().size() != 0) {
            if (isExportacion()) {

                for (int i = 0; i < getTotales().size(); i++) {
                    if (i > 7) {
                        if (getTotales().get(i).getTitulo().equalsIgnoreCase("VALOR TOTAL:")) {
                            datos.add("$ " + cambiarFormato(getTotales().get(i).getValor()));
                        } else {
                            datos.add(cambiarFormato(getTotales().get(i).getValor()));
                        }
                        titulos.add(getTotales().get(i).getTitulo());

                    } else {
                        if (getTotales().get(i).getValor() != null
                                && !getTotales().get(i).getValor().toString().trim().equalsIgnoreCase("0")
                                && !getTotales().get(i).getValor().toString().trim().equalsIgnoreCase("0.00")) {

                            datos.add(cambiarFormato(getTotales().get(i).getValor()));
                            titulos.add(getTotales().get(i).getTitulo());

                        }
                    }
                }
            } else {

                for (int i = 0; i < getTotales().size(); i++) {
                    if (i < 7) {

                        datos.add(cambiarFormato(getTotales().get(i).getValor()));
                        titulos.add(getTotales().get(i).getTitulo());

                    } else {
                        if (getTotales().get(i).getValor() != null//) {
                                && !getTotales().get(i).getValor().toString().trim().equalsIgnoreCase("0")
                                && !getTotales().get(i).getValor().toString().trim().equalsIgnoreCase("0.00")) {

                            if (i == 7) {//cuando es propina
                                datos.add(cambiarFormato(getTotales().get(i).getValor()));
                                titulos.add(getTotales().get(i).getTitulo());
                            } else {
                                datos.add("$ " + cambiarFormato(getTotales().get(i).getValor()));
                                titulos.add(getTotales().get(i).getTitulo());
                            }
                        }
                    }
                }
            }
            String[] anArray = new String[titulos.size()];
            tablaVertical.setTitulos(titulos.toArray(anArray));
            tablaVertical.setContenidos(datos.toArray());
            tablaVertical.setAlineamientos(alineamientos);
            tablaVertical.llenarTabla(true);
            tablaVertical.setPosicion(2);
            tablaVertical.setAnchoTabla(42);
            tablaVertical.setTamanos(tamanos);
            documento.add(tablaVertical.getTabla());

            if (tieneSubsidio()) {
                espacionBlanco(1);
                tablaVertical.limpiar();
                datos = new Vector();
                titulos = new Vector<String>();
                titulos.add("TOTAL SIN SUBSIDIO");
                titulos.add("AHORRO POR SUBSIDIO");
                datos.add(cambiarFormato(getBus().getInfoFactura().getTotalSubsidio().doubleValue() + getBus().getInfoFactura().getImporteTotal().doubleValue()));
                datos.add(cambiarFormato(getBus().getInfoFactura().getTotalSubsidio()));

                anArray = new String[titulos.size()];
                tablaVertical.setTitulos(titulos.toArray(anArray));
                tablaVertical.setContenidos(datos.toArray());
                tablaVertical.setAlineamientos(alineamientos);
                tablaVertical.llenarTabla(false);
                tablaVertical.setPosicion(2);

                tablaVertical.setAnchoTabla(42);
                tablaVertical.setTamanos(tamanos);
                documento.add(tablaVertical.getTabla());

            }

        }
    }


    /**
     * Metodo para cargar los totales.
     *
     * @param bus
     */
    private void cargarTotales(Factura bus) {
        //    private static String[] TOTALES = {"SUBTOTAL 12%:", "SUBTOTAL 0%:", "SUBTOTAL No Obj IVA:", "SUBTOTAL:", "DESCUENTO:", "ICE:", "IVA 12%:", "PROPINA:", "FLETE INTERNACIONAL:", "SEGURO INTERNACIONAL:", "GASTOS ADUANEROS:", "OTROS GASTOS DE TRANSPORTE:", "VALOR TOTAL:"};
        BigDecimal valor = new BigDecimal(0);
        if ((bus.getInfoFactura().getTotalConImpuestos() != null) && (bus.getInfoFactura().getTotalConImpuestos().getTotalImpuesto().size() > 0)) {
            for (Factura.InfoFactura.TotalConImpuestos.TotalImpuesto a : bus.getInfoFactura().getTotalConImpuestos().getTotalImpuesto()) {

                if (a.getCodigo().compareTo("2") == 0) {

                    if (a.getCodigoPorcentaje().compareTo("0") == 0) {
                        System.out.println("entro 0");
                        getTotales().get(1).setValor(a.getBaseImponible());
                        getTotales().get(1).setTitulo("SUBTOTAL 0%:");

                    } else if (a.getCodigoPorcentaje().compareTo("6") == 0) {

                        getTotales().get(2).setValor(a.getBaseImponible());

                    } else {

                        getTotales().get(0).setValor(a.getBaseImponible());
                        getTotales().get(6).setValor(a.getValor());
                        String txtValor = UtilTabla.ObtenerTabla("Tabla18", a.getCodigoPorcentaje().trim());
                        getTotales().get(0).setTitulo("SUBTOTAL " + txtValor + ":");
                        getTotales().get(6).setTitulo("IVA " + txtValor + ":");
                        valor = a.getBaseImponible();
                    }

                }
                if (a.getCodigo().compareTo("3") == 0) {
                    getTotales().get(5).setValor(a.getValor());
                }
            }
        }

        getTotales().get(3).setValor(bus.getInfoFactura().getTotalSinImpuestos());
        getTotales().get(4).setValor(bus.getInfoFactura().getTotalDescuento());
        getTotales().get(7).setValor(bus.getInfoFactura().getPropina());
        getTotales().get(8).setValor(bus.getInfoFactura().getFleteInternacional());
        getTotales().get(9).setValor(bus.getInfoFactura().getSeguroInternacional());
        getTotales().get(10).setValor(bus.getInfoFactura().getGastosAduaneros());
        getTotales().get(11).setValor(bus.getInfoFactura().getGastosTransporteOtros());
        getTotales().get(12).setValor(bus.getInfoFactura().getImporteTotal());

    }




    /**
     * Metodo que permite ingresar detalle del documento.
     *
     * @throws DocumentException
     */
    private int[] obenerTamanoDetalleDocumento(boolean issubsidio) {
        int[] tamanosDefaul = issubsidio ? new int[]{12, 12, 35, 10, 10, 12, 15, 15, 10, 15} : new int[]{12, 12, 35, 10, 10, 12, 10, 15};
        String formato = issubsidio ? this.formatoDetallColSizeSubsidio : this.formatoDetallColSizeSinSubsidio;
        if (formato == null || formato.isEmpty()) {
            return tamanosDefaul;
        }
        try {
            String[] NumColColumnas = formato.split(":");
            int numCol = Integer.parseInt(NumColColumnas[0]);
            String[] tamanosCol = NumColColumnas[1].split(",");
            if (numCol != tamanosCol.length) {
                return tamanosDefaul;
            }
            int[] numbers = new int[tamanosCol.length];
            for (int i = 0; i < tamanosCol.length; i++) {
                numbers[i] = Integer.parseInt(tamanosCol[i]);
            }
            return numbers;

        } catch (NumberFormatException ex) {
            System.out.println("Erro tamanos: " + ex);
            return tamanosDefaul;
        }

    }

    private void detalleDocumento() throws DocumentException {

        int i = 1;
        if (getBus().getDetalles() != null && getBus().getDetalles().getDetalle().size() > 0) {
            boolean isSubsidio = tieneSubsidio();
            espacionBlanco(1);
            Vector vector = new Vector();
            int[] alineamientos = null;
            int[] tamanos = null;
            if (isSubsidio) {
                tablaHorizontal.setColumnas(10);
                tablaHorizontal.setTitulos("Cód", "Cód Aux", "Descripción", "Cant.", "U. Med.", "P. Unit", "Subsidio", "P. sin Subsidio", "Desct", "Total");
                alineamientos = new int[]{0, 0, 2, 2, 2, 2, 2, 2, 2, 2};
                tamanos = obenerTamanoDetalleDocumento(isSubsidio);
            } else {
                tablaHorizontal.setColumnas(8);
                tablaHorizontal.setTitulos("Cód", "Cód Aux", "Descripción", "Cant.", "U. Med.", "P. Unit", "Desct", "Total");
                alineamientos = new int[]{0, 0, 0, 2, 2, 2, 2, 2};
                tamanos = obenerTamanoDetalleDocumento(isSubsidio);
            }
            boolean tieneSubsidioInterno = false;
            for (Factura.Detalles.Detalle a : getBus().getDetalles().getDetalle()) {
                tieneSubsidioInterno = false;
                List datos1 = new ArrayList();
                datos1.add(a.getCodigoPrincipal());
                datos1.add(a.getCodigoAuxiliar());
                datos1.add(a.getDescripcion());
                datos1.add(String.valueOf(a.getCantidad()));
                datos1.add(String.valueOf(isNull(a.getUnidadMedida(), "")));
                datos1.add(String.valueOf(a.getPrecioUnitario()));

                if (isSubsidio) { //el doucmento tiene subsidio
                    tieneSubsidioInterno = (a.getPrecioSinSubsidio() != null); //el detalle tien sussudio
                    if (tieneSubsidioInterno) {
                        datos1.add(String.valueOf(a.getPrecioSinSubsidio().doubleValue() - a.getPrecioUnitario().doubleValue()));
                        datos1.add(String.valueOf(a.getPrecioSinSubsidio()));
                    } else {
                        datos1.add("0.00");
                        datos1.add("0.00");
                    }
                }
                datos1.add(String.valueOf(a.getDescuento()));

                datos1.add(cambiarFormato(a.getPrecioTotalSinImpuesto()));
                // System.out.println();

                vector.add(datos1);

                if (a.getDetallesAdicionales() != null && !a.getDetallesAdicionales().getDetAdicional().isEmpty()) {
                    for (Factura.Detalles.Detalle.DetallesAdicionales.DetAdicional b : a.getDetallesAdicionales().getDetAdicional()) {
                        datos1 = new ArrayList();

                        datos1.add("");
                        datos1.add("");

                        datos1.add(isNull(b.getNombre(), "") + ": " + isNull(b.getValor(), ""));

                        if (isSubsidio) {
                            datos1.add("");
                            datos1.add("");
                        }

                        datos1.add("");
                        datos1.add("");
                        datos1.add("");
                        datos1.add("");
                        datos1.add("");
                        vector.add(datos1);
                    }
                }
            }

            Object[] datos = new Object[vector.size()];
            for (int k = 0; k < vector.size(); k++) {
                datos[k] = vector.get(k);
            }

            tablaHorizontal.setContenidos(datos);
            tablaHorizontal.setAlineamientos(alineamientos);
            tablaHorizontal.setTamanos(tamanos);
            tablaHorizontal.llenarTabla();
            tablaHorizontal.setPosicion(2);
            tablaHorizontal.setAnchoTabla(100);
            documento.add(tablaHorizontal.getTabla());

            espacionBlanco(1);
            documento.add(lineaNormal.getLinea());

        }
    }

    /**
     * Metodo que permite ingresar informacion adicional si esque existe.
     *
     */
    private void informacionAdicional() throws DocumentException {
        if (getBus().getInfoAdicional() != null && getBus().getInfoAdicional().getCampoAdicional().size() > 0) {
            String titulos[] = new String[getBus().getInfoAdicional().getCampoAdicional().size()];
            Object contenido[] = new Object[getBus().getInfoAdicional().getCampoAdicional().size()];
            int i = 0;
            for (Factura.InfoAdicional.CampoAdicional a : getBus().getInfoAdicional().getCampoAdicional()) {
                if (datosCliente(LLAVE_CORREO_CLIENTE, a.getNombre())) {
                    if (datosCliente(LLAVE_DIRECCION_CLIENTE, a.getNombre())) {
                        if (datosCliente(LLAVE_TELEFONO_CLIENTE, a.getNombre())) {
                            titulos[i] = a.getNombre() + ":";
                            contenido[i] = a.getValue();
                        }
                    }

                }

                i++;
            }

            tablaVertical.limpiar();
            tablaVertical.setTitulos(titulos);
            tablaVertical.setContenidos(contenido);
            tablaVertical.setAlineamientos(new int[]{0, 0});
            tablaVertical.llenarTabla(false);
            tablaVertical.setTamanos(new int[]{18, 82});
            tablaVertical.setPosicion(2);
            tablaVertical.setAnchoTabla(100);

            espacionBlanco(1);
            titulo2.setTexto("INFORMACIÓN ADICIONAL");
            titulo2.CambiarPosicion((float) 9.5);
            documento.add(titulo2.getElemento());
            lineaCentrada.cambiarTamana(76);
            documento.add(lineaCentrada.getLinea());
            espacionBlanco(1);
            documento.add(tablaVertical.getTabla());
        }
    }


    private void informacionExportacion() throws DocumentException {
        if (isExportacion()) {
            espacionBlanco(1);
            titulo2.setTexto("INFORMACIÓN EXPORTACIÓN");
            titulo2.CambiarPosicion((float) 9.5);
            documento.add(titulo2.getElemento());

            lineaCentrada.cambiarTamana(72);
            documento.add(lineaCentrada.getLinea());
            espacionBlanco(1);

            tablaVertical.limpiar(4);
            tablaVertical.setTitulos("Términos de Negociación:", "Lugar de Negociación:", "País Origen:", "Puerto Embarque:", "País Destino:", "Puerto Destino:", "País Adquisición:");
            tablaVertical.setContenidos(new Object[]{getBus().getInfoFactura().getIncoTermFactura(), getBus().getInfoFactura().getLugarIncoTerm(),
                    UtilTabla.ObtenerTabla("Tabla25", getBus().getInfoFactura().getPaisOrigen()),
                    getBus().getInfoFactura().getPuertoEmbarque(),
                    UtilTabla.ObtenerTabla("Tabla25", getBus().getInfoFactura().getPaisDestino()),
                    getBus().getInfoFactura().getPuertoDestino(), UtilTabla.ObtenerTabla("Tabla25", getBus().getInfoFactura().getPaisAdquisicion())});

            tablaVertical.setAlineamientos(new int[]{0, 0, 0, 0, 0, 0});
            tablaVertical.llenarTabla(false);
            tablaVertical.setTamanos(new int[]{25, 30, 25, 25});
            tablaVertical.setPosicion(2);
            tablaVertical.setAnchoTabla(100);
            documento.add(tablaVertical.getTabla());
        }

    }



    public String cambiarFormato(Object obj) {
        NumberFormat nf = NumberFormat.getInstance();
        nf = NumberFormat.getCurrencyInstance(Locale.US);
        String numero = nf.format(obj).trim();
        return numero.replace('$', ' ');
    }


    private String isNull(String valor, String remplazo) {
        return (valor == null) ? remplazo : valor;
    }


    /**
     * Metodo que permite ingresar informacion del cliente.
     *
     * @throws DocumentException
     */
    private void informaicionCliente() throws DocumentException {

        titulo2.getElementoNegro();
        titulo2.setTexto("INFORMACIÓN CONTRIBUYENTE");
        titulo2.CambiarPosicion((float) 9.5);
        documento.add(titulo2.getElemento());
        lineaCentrada.cambiarTamana(70);
        documento.add(lineaCentrada.getLinea());
        espacionBlanco(1);
        tablaVertical.limpiar(4);
        // System.out.println(getBus().getInfoFactura().getGuiaRemision());
        if (getBus().getInfoFactura().getGuiaRemision() == null) {

            tablaVertical.setTitulos("RUC/CI/Pasaporte:", "Teléfono:", "Razón Social:", "Fecha Emisión:", "Dirección:",
                    "Correo(s):", "");
            Object[] datos = {
                    getBus().getInfoFactura().getIdentificacionComprador(),
                    datosCliente(LLAVE_TELEFONO_CLIENTE),
                    getBus().getInfoFactura().getRazonSocialComprador(),
                    getBus().getInfoFactura().getFechaEmision(),
                    (getBus().getInfoFactura().getDireccionComprador() == null) ? datosCliente(LLAVE_DIRECCION_CLIENTE) : getBus().getInfoFactura().getDireccionComprador(),
                    datosCliente(LLAVE_CORREO_CLIENTE), ""
            };
            tablaVertical.setContenidos(datos);
        } else {

            tablaVertical.setTitulos("RUC/CI/Pasaporte:", "Teléfono:", "Razón Social:", "Fecha Emisión:", "Dirección:",
                    "Guía Remisión:", "Correo(s):", "");
            Object[] datos = {
                    getBus().getInfoFactura().getIdentificacionComprador(),
                    datosCliente(LLAVE_TELEFONO_CLIENTE),
                    getBus().getInfoFactura().getRazonSocialComprador(),
                    getBus().getInfoFactura().getFechaEmision(),
                    (getBus().getInfoFactura().getDireccionComprador() == null) ? datosCliente(LLAVE_DIRECCION_CLIENTE) : getBus().getInfoFactura().getDireccionComprador(),
                    getBus().getInfoFactura().getGuiaRemision(),
                    datosCliente(LLAVE_CORREO_CLIENTE), ""
            };
            tablaVertical.setContenidos(datos);
        }

        tablaVertical.setAlineamientos(new int[]{0, 0, 0, 0, 0,});
        tablaVertical.llenarTabla(false);
        tablaVertical.setTamanos(new int[]{16, 40, 14, 35});
        tablaVertical.setPosicion(2);
        tablaVertical.setAnchoTabla(100);
        documento.add(tablaVertical.getTabla());
    }


    /**
     * Metodo para crear espacios en blanco.
     *
     * @param espacios
     */
    private void espacionBlanco(int espacios) {
        try {
            for (int i = 0; i < espacios; i++) {
                documento.add(espacioBlanco.getElemento());
            }
        } catch (DocumentException e) {
            log.error(" informacion espacionBlanco mal", e);
        }
    }

    public boolean datosCliente(String clave, String campo) {
        byte count = 0;
        for (String datos : clave.split(",")) {
            if (campo.toLowerCase().contains(datos.toLowerCase())) {
                count++;

            }

        }
        return count <= 0;
    }

    /**
     * Metodo para sacar la informacion del cliente dentro del sistema de
     * impresiones
     *
     * @param llave
     * @return
     */
    private String datosCliente(String llave) {
        try {
            for (Factura.InfoAdicional.CampoAdicional a : getBus().getInfoAdicional().getCampoAdicional()) {

                for (String datos : llave.split(",")) {
                    if (a.getNombre().toLowerCase().contains(datos)) {

                        return a.getValue();
                    }
                }

            }
        } catch (Exception e) {
        }
        return null;
    }




    /**
     * Informacion de pie de pagina.
     */
    private void informacionPiePaguina() {
        encabezado.setTextofondo("");
        encabezado.generarMarcaAgua();
        java.util.Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        encabezado.setEncabezado("Creado el: " + formateador.format(ahora));
        documento.getWriter().setPageEvent(encabezado);
    }


    /**
     * Metodo para inicialzar totales.
     */
    private void inicializaTotales() {

        TotalFactura totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[0]);
        getTotales().add(0, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[1]);
        getTotales().add(1, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[2]);
        getTotales().add(2, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[3]);
        getTotales().add(3, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[4]);
        getTotales().add(4, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[5]);
        getTotales().add(5, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[6]);
        getTotales().add(6, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[7]);
        getTotales().add(7, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[8]);
        getTotales().add(8, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[9]);
        getTotales().add(9, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[10]);
        getTotales().add(10, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[11]);
        getTotales().add(11, totalFactura);

        totalFactura = new TotalFactura();
        totalFactura.setTitulo(TOTALES[12]);
        getTotales().add(12, totalFactura);
    }

    @Getter
    @Setter
    public class TotalFactura {

        private String titulo;
        private BigDecimal valor;

        /**
         * Metodo para crear el objeto.
         */
        public TotalFactura() {
            setTitulo("");
            setValor(new BigDecimal(0));
        }
    }


    private boolean isExportacion() {

        return getBus().getInfoFactura().getComercioExterior() != null && getBus().getInfoFactura().getComercioExterior().trim().length() > 0;
    }

    private boolean tieneSubsidio() {
        return getBus().getInfoFactura().getTotalSubsidio() != null
                && !getBus().getInfoFactura().getTotalSubsidio().toString().trim().equalsIgnoreCase("0")
                && !getBus().getInfoFactura().getTotalSubsidio().toString().trim().equalsIgnoreCase("0.00");
    }


}
