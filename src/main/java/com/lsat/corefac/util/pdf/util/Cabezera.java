package com.lsat.corefac.util.pdf.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.lsat.corefac.model.util.docs.CR.ComprobanteRetencion;
import com.lsat.corefac.model.util.docs.FE.Factura;
import com.lsat.corefac.model.util.docs.GR.GuiaRemision;
import com.lsat.corefac.model.util.docs.LC.LiquidacionCompra;
import com.lsat.corefac.model.util.docs.NC.NotaCredito;
import com.lsat.corefac.model.util.docs.ND.NotaDebito;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Cabezera extends PdfPageEventHelper {

    private CodigoBarra codigoBarra = new CodigoBarra();

    private EspacioBlanco espacioBlanco = new EspacioBlanco();

    private TablaVertical tablaVertical = new TablaVertical();

    private Imagen imagen = new Imagen();

    private Titulo2 titulo2 = new Titulo2();

    private Titulo3 titulo3 = new Titulo3();

    private Titulo1 titulo1 = new Titulo1();

    private Titulo4 titulo4 = new Titulo4();

    private Object bus;

    private String numeroAutorizacion;

    private String fechaAutorizacion;

    private String ambiente;

    private String emision;

    private String claveAcceso;

    private String direccionLogotipo;

    private String ruc;

    private String razonSocial;

    private PdfWriter writer;

    private String DirEstablecimiento;

    private String DirMatriz;

    private String ContribuyenteEspecial;

    private String ObligadoContabilidad;

    private String nombreDocumento;

    private String Estab;

    private String PtoEmi;

    private String Secuencial;

    private String regimenMicroempresas;

    private String agenteRetencion;

    private String contribuyenteRimpe;

    public void onStartPage(PdfWriter writer, Document document) {
        super.onStartPage(writer, document);
        try {
            addHeaderTable(document);
        } catch (DocumentException e) {
            log.error("Error al onStartPage: " + e.toString());
        }
    }


    private float clacularTamano(String texto) {
        float res = 0.0F, tam = texto.trim().length();
        if (tam <= 30.0F) {
            res = 12.0F;
        } else if (tam > 30.0F && tam < 45.0F) {
            res = 9.0F;
        } else {
            res = 8.0F;
        }
        return res;
    }

    public void addHeaderTable(Document document) throws DocumentException {
        PdfPTable tablePanelInicial = new PdfPTable(2);
        tablePanelInicial.setWidthPercentage(100.0F);
        tablePanelInicial.getDefaultCell().setBorder(0);
        tablePanelInicial.getDefaultCell().setBorderWidthTop(1.0F);
        tablePanelInicial.getDefaultCell().setBorderWidthBottom(1.0F);
        PdfPTable tablePanelIzquierdo = new PdfPTable(1);
        tablePanelIzquierdo.setWidthPercentage(50.0F);
        tablePanelIzquierdo.getDefaultCell().setBorder(0);
        PdfPTable tablePanelDerecho = new PdfPTable(1);
        tablePanelDerecho.setWidthPercentage(100.0F);
        tablePanelDerecho.getDefaultCell().setBorder(0);
        this.titulo2.getElementoNegro();
        this.titulo3.getElementoNegro();
        String nombreLogo = getRuc();
        this.imagen.setPath(this.direccionLogotipo);

        this.imagen.setScala(55.0F);
        this.imagen.escribe();
        PdfPTable image = new PdfPTable(1);
        image.setWidthPercentage(100.0F);
        PdfPCell cell = new PdfPCell(this.imagen.getImagen());
        cell.setBorder(0);
        cell.setHorizontalAlignment(1);
        cell.setVerticalAlignment(5);
        cell.setMinimumHeight(100.0F);
        image.addCell(cell);
        tablePanelIzquierdo.addCell(image);
        this.titulo2.getElementoVerde();
        this.titulo2.setTexto(getRazonSocial().toUpperCase());
        this.titulo2.CambiarPosicion(clacularTamano(getRazonSocial().toUpperCase()));
        tablePanelIzquierdo.addCell((Phrase) this.titulo2.getElemento(2));
        this.tablaVertical.limpiar();
        this.tablaVertical.setTitulos(new String[]{"Dir. Matriz:", "Dir. Sucursal:"});
        Object[] datos1 = {getDirMatriz(), getDirEstablecimiento()};
        this.tablaVertical.setContenidos(datos1);
        this.tablaVertical.setAlineamientos(new int[]{0, 0});
        this.tablaVertical.llenarTabla(false);
        this.tablaVertical.setPosicion(2);
        this.tablaVertical.setTamanos(new int[]{25, 75});
        this.tablaVertical.setAnchoTabla(100);
        tablePanelIzquierdo.addCell(this.tablaVertical.getTabla());
        this.tablaVertical.limpiar();
        if (this.ContribuyenteEspecial != null && this.ContribuyenteEspecial.length() > 0) {
            Object[] datos2 = {getContribuyenteEspecial(), getObligadoContabilidad()};
            this.tablaVertical.setTitulos(new String[]{"Contribuyente Especial Nro.:", "Obligado a llevar Contabilidad:"});
            this.tablaVertical.setContenidos(datos2);
        } else {
            Object[] datos2 = {getObligadoContabilidad()};
            this.tablaVertical.setTitulos(new String[]{"Obligado a llevar Contabilidad:"});
            this.tablaVertical.setContenidos(datos2);
        }
        this.tablaVertical.setAlineamientos(new int[]{0, 0});
        this.tablaVertical.llenarTabla(false);
        this.tablaVertical.setPosicion(2);
        this.tablaVertical.setTamanos(new int[]{90, 40});
        this.tablaVertical.setAnchoTabla(100);
        tablePanelIzquierdo.addCell(this.tablaVertical.getTabla());
        if (this.regimenMicroempresas != null && !this.regimenMicroempresas.isEmpty()) {
            this.titulo4.setTexto(getRegimenMicroempresas().toUpperCase());
            this.titulo4.getElemento(1);
            PdfPCell rucNombreDoc2 = new PdfPCell((Phrase) this.titulo4.getElemento());
            rucNombreDoc2.setHorizontalAlignment(0);
            rucNombreDoc2.setBorder(0);
            tablePanelIzquierdo.addCell(rucNombreDoc2);
        }
        if (this.agenteRetencion != null && !this.agenteRetencion.isEmpty()) {
            this.tablaVertical.limpiar();
            Object[] datos2 = {getAgenteRetencion()};
            this.tablaVertical.setTitulos(new String[]{"Agente de Retención Resolución No.:"});
            this.tablaVertical.setContenidos(datos2);
            this.tablaVertical.setAlineamientos(new int[]{0, 0});
            this.tablaVertical.llenarTabla(false);
            this.tablaVertical.setPosicion(2);
            this.tablaVertical.setTamanos(new int[]{90, 40});
            this.tablaVertical.setAnchoTabla(100);
            tablePanelIzquierdo.addCell(this.tablaVertical.getTabla());
        }
        if (this.contribuyenteRimpe != null && !this.contribuyenteRimpe.isEmpty()) {
            this.tablaVertical.limpiar();
            Object[] datos2 = {" "};
            this.tablaVertical.setTitulos(new String[]{getContribuyenteRimpe()});
            this.tablaVertical.setContenidos(datos2);
            this.tablaVertical.setAlineamientos(new int[]{0, 0});
            this.tablaVertical.llenarTabla(false);
            this.tablaVertical.setPosicion(2);
            this.tablaVertical.setTamanos(new int[]{90, 40});
            this.tablaVertical.setAnchoTabla(100);
            tablePanelIzquierdo.addCell(this.tablaVertical.getTabla());
        }
        tablePanelInicial.addCell(tablePanelIzquierdo);
        this.titulo1.setTexto("RUC: " + getRuc());
        this.titulo1.getElemento(1);
        PdfPCell rucCell = new PdfPCell((Phrase) this.titulo1.getElemento());
        rucCell.setHorizontalAlignment(1);
        rucCell.setBorder(0);
        tablePanelDerecho.addCell(rucCell);
        this.titulo1.setTexto(getNombreDocumento().toUpperCase());
        this.titulo1.getElemento(1);
        this.titulo1.getElementoBlanco();
        PdfPCell rucNombreDoc = new PdfPCell((Phrase) this.titulo1.getElemento());
        rucNombreDoc.setHorizontalAlignment(1);
        rucNombreDoc.setBackgroundColor(BaseColor.DARK_GRAY);
        rucNombreDoc.setBorder(0);
        tablePanelDerecho.addCell(rucNombreDoc);
        tablePanelDerecho.addCell((Phrase) this.espacioBlanco.getElemento());
        this.titulo1.setTexto("No.: " + getEstab() + "-" + getPtoEmi() + "-"
                + getSecuencial());
        this.titulo1.getElementoVerde();
        tablePanelDerecho.addCell((Phrase) this.titulo1.getElemento());
        tablePanelDerecho.addCell((Phrase) this.espacioBlanco.getElemento());
        this.tablaVertical.limpiar(1);
        int[] tamanos2 = {50};
        int[] alineamientos = {0, 0};
        this.tablaVertical.setTamanos(tamanos2);
        this.tablaVertical.setTitulos(new String[]{"Nro Autorización"});
        Object[] datos10 = {this.numeroAutorizacion};
        this.tablaVertical.setContenidos(datos10);
        this.tablaVertical.setAlineamientos(alineamientos);
        this.tablaVertical.llenarTabla(false);
        this.tablaVertical.setPosicion(0);
        this.tablaVertical.setAnchoTabla(65);
        tablePanelDerecho.addCell(this.tablaVertical.getTabla());
        if (getFechaAutorizacion() != null && !getFechaAutorizacion().isEmpty()) {
            this.tablaVertical.limpiar();
            Object[] datos3 = {getFechaAutorizacion()};
            this.tablaVertical.setTitulos(new String[]{"Fecha y Hora de Autorización"});
            this.tablaVertical.setContenidos(datos3);
            this.tablaVertical.llenarTabla(false);
            this.tablaVertical.setPosicion(0);
            this.tablaVertical.setAnchoTabla(65);
            tablePanelDerecho.addCell(this.tablaVertical.getTabla());
        }
        this.tablaVertical.limpiar();
        this.tablaVertical.setColumnas(4);
        Object[] datos4 = {getAmbiente().toUpperCase(), getEmision().toUpperCase()};
        this.tablaVertical.setTitulos(new String[]{"Ambiente:", "Emisión"});
        this.tablaVertical.setContenidos(datos4);
        this.tablaVertical.setAnchoTabla(100);
        this.tablaVertical.setTamanos(new int[]{10, 10, 10, 10});
        this.tablaVertical.llenarTabla(false);
        tablePanelDerecho.addCell(this.tablaVertical.getTabla());
        tablePanelDerecho.addCell((Phrase) this.espacioBlanco.getElemento());
        this.titulo3.setTexto("Clave de Acceso:");
        this.titulo3.getElementoNegro();
        tablePanelDerecho.addCell((Phrase) this.titulo3.getElemento());
        getCodigoBarra().setTexto(getClaveAcceso());
        tablePanelDerecho.addCell(getCodigoBarra().getCodigoBarra(getWriter().getDirectContent()));
        tablePanelInicial.addCell(tablePanelDerecho);
        document.add((Element) tablePanelInicial);
        document.add((Element) this.espacioBlanco.getElemento());
    }

    public Object getBus() {
        return this.bus;
    }

    public void setBus(Object bus) {
        if (bus instanceof Factura) {
            this.bus = (Factura) bus;
            llenarDatos((Factura) this.bus);
            nombreDocumento = "Factura";
        } else if (bus instanceof NotaCredito) {
            this.bus = (NotaCredito) bus;
            llenarDatos((NotaCredito) this.bus);
            nombreDocumento = "Nota Crédito";
        } else if (bus instanceof NotaDebito) {
            this.bus = (NotaDebito) bus;
            llenarDatos((NotaDebito) this.bus);
            nombreDocumento = "NOTA DÉBITO";
        } else if (bus instanceof ComprobanteRetencion) {
            this.bus = (ComprobanteRetencion) bus;
            llenarDatos((ComprobanteRetencion) this.bus);
            nombreDocumento = "COMPROBANTE DE RETENCIÓN";
        } else if (bus instanceof GuiaRemision) {
            this.bus = (GuiaRemision) bus;
            llenarDatos((GuiaRemision) this.bus);
            nombreDocumento = "GUIA DE REMISIÓN";
        } else if (bus instanceof LiquidacionCompra) {
            this.bus = (LiquidacionCompra) bus;
            llenarDatos((LiquidacionCompra) this.bus);
            nombreDocumento = "LIQUIDACIÓN DE COMPRA DE BIENES Y PRESTACIÓN DE SERVICIOS";
        }
        /*else if (bus instanceof ec.procesoselectronicos.documentos.CRN.ComprobanteRetencion) {
            this.bus = (ec.procesoselectronicos.documentos.CRN.ComprobanteRetencion) bus;
            llenarDatos((ec.procesoselectronicos.documentos.CRN.ComprobanteRetencion) this.bus);
            nombreDocumento = "COMPROBANTE DE RETENCIÓN";
        }*/
    }

    public void llenarDatos(Factura bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoFactura().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoFactura().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoFactura().getObligadoContabilidad().toString());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
        setContribuyenteRimpe(bus.getInfoTributaria().getContribuyenteRimpe());
    }

    public void llenarDatos(NotaCredito bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoNotaCredito().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoNotaCredito().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoNotaCredito().getObligadoContabilidad());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
    }

    public void llenarDatos(NotaDebito bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoNotaDebito().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoNotaDebito().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoNotaDebito().getObligadoContabilidad());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
    }

    public void llenarDatos(ComprobanteRetencion bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoCompRetencion().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoCompRetencion().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoCompRetencion().getObligadoContabilidad());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
    }

    public void llenarDatos(GuiaRemision bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoGuiaRemision().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoGuiaRemision().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoGuiaRemision().getObligadoContabilidad());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
    }

    public void llenarDatos(LiquidacionCompra bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoLiquidacionCompra().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoLiquidacionCompra().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoLiquidacionCompra().getObligadoContabilidad().toString());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
    }

   /* public void llenarDatos(ec.procesoselectronicos.documentos.CRN.ComprobanteRetencion bus) {
        setAmbiente(bus.getInfoTributaria().getAmbiente());
        setClaveAcceso(bus.getInfoTributaria().getClaveAcceso());
        setContribuyenteEspecial(bus.getInfoCompRetencion().getContribuyenteEspecial());
        setDirEstablecimiento(bus.getInfoCompRetencion().getDirEstablecimiento());
        setDirMatriz(bus.getInfoTributaria().getDirMatriz());
        setEstab(bus.getInfoTributaria().getEstab());
        setObligadoContabilidad(bus.getInfoCompRetencion().getObligadoContabilidad().value());
        setPtoEmi(bus.getInfoTributaria().getPtoEmi());
        setRazonSocial(bus.getInfoTributaria().getRazonSocial().toUpperCase());
        setRuc(bus.getInfoTributaria().getRuc());
        setSecuencial(bus.getInfoTributaria().getSecuencial());
        setAgenteRetencion(bus.getInfoTributaria().getAgenteRetencion());
        // setRegimenMicroempresas(bus.getInfoTributaria().getRegimenMicroempresas());
        setContribuyenteRimpe(bus.getInfoTributaria().getContribuyenteRimpe());

    }*/

    public void limpiarDatos() {
    }

    public CodigoBarra getCodigoBarra() {
        return this.codigoBarra;
    }

    public void setCodigoBarra(CodigoBarra codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public String getNumeroAutorizacion() {
        return this.numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getFechaAutorizacion() {
        return this.fechaAutorizacion;
    }

    public void setFechaAutorizacion(String fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public String getAmbiente() {
        return this.ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getEmision() {
        return this.emision;
    }

    public void setEmision(String emision) {
        this.emision = emision;
    }

    public String getClaveAcceso() {
        return this.claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getDireccionLogotipo() {
        return this.direccionLogotipo;
    }

    public void setDireccionLogotipo(String direccionLogotipo) {
        this.direccionLogotipo = direccionLogotipo;
    }

    public PdfWriter getWriter() {
        return this.writer;
    }

    public void setWriter(PdfWriter writer) {
        this.writer = writer;
    }

    private Object DirMatriz() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getRuc() {
        return this.ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDirEstablecimiento() {
        return this.DirEstablecimiento;
    }

    public void setDirEstablecimiento(String DirEstablecimiento) {
        this.DirEstablecimiento = DirEstablecimiento;
    }

    public String getDirMatriz() {
        return this.DirMatriz;
    }

    public void setDirMatriz(String DirMatriz) {
        this.DirMatriz = DirMatriz;
    }

    public String getContribuyenteEspecial() {
        return this.ContribuyenteEspecial;
    }

    public void setContribuyenteEspecial(String ContribuyenteEspecial) {
        this.ContribuyenteEspecial = ContribuyenteEspecial;
    }

    public String getObligadoContabilidad() {
        return this.ObligadoContabilidad;
    }

    public void setObligadoContabilidad(String ObligadoContabilidad) {
        this.ObligadoContabilidad = ObligadoContabilidad;
    }

    public String getNombreDocumento() {
        return this.nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getEstab() {
        return this.Estab;
    }

    public void setEstab(String Estab) {
        this.Estab = Estab;
    }

    public String getPtoEmi() {
        return this.PtoEmi;
    }

    public void setPtoEmi(String PtoEmi) {
        this.PtoEmi = PtoEmi;
    }

    public String getSecuencial() {
        return this.Secuencial;
    }

    public void setSecuencial(String Secuencial) {
        this.Secuencial = Secuencial;
    }

    public String getRegimenMicroempresas() {
        return this.regimenMicroempresas;
    }

    public void setRegimenMicroempresas(String regimenMicroempresas) {
        this.regimenMicroempresas = regimenMicroempresas;
    }

    public String getAgenteRetencion() {
        return this.agenteRetencion;
    }

    public void setAgenteRetencion(String agenteRetencion) {
        this.agenteRetencion = agenteRetencion;
    }

    public String getContribuyenteRimpe() {
        return this.contribuyenteRimpe;
    }

    public void setContribuyenteRimpe(String contribuyenteRimpe) {
        this.contribuyenteRimpe = contribuyenteRimpe;
    }
}
