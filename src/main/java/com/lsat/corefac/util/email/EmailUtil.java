package com.lsat.corefac.util.email;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.Properties;

@Getter
@Setter
public class EmailUtil {

    private String asunto;
    private String contenido;
    private List<String> listEmails;
    private Map<String, String> mapParametros;
    private String contenidoKey;
    private String asuntoKey;
    private String estilos;
    private String estilosKey;
    private List<String> listaAdjuntos;
    private Properties propiedades;
    private String nombreDocumento;

}
