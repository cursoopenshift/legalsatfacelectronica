package com.lsat.corefac.util.email;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


@Getter
@Setter
@Slf4j
public class EnvioEmailThread extends Thread {


    private String host;
    private String port;
    private String user;
    private String p;
    private String auth;
    private String startTls;
    private EmailUtil email;

    public EnvioEmailThread(EmailUtil email) {
        this.email = email;
    }

    public void run() {
        final String usuario = user;
        final String ps = p;
        Session session = Session.getInstance(this.email.getPropiedades(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, ps);
            }
        });
        enviarCorreo(session);
    }

    public void enviarCorreo(Session session) {
        try {
            System.out.println("entro a enviar correo");
            String content = this.email.getContenido();
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(user));
            message.setContent(content, "text/html");
            message.setSubject(this.email.getAsunto());

            if (this.email.getListaAdjuntos().size() > 0) {
                MimeMultipart multipart = new MimeMultipart();
                BodyPart htmlPart = new MimeBodyPart();
                htmlPart.setContent(content, "text/html");
                for (String a : this.email.getListaAdjuntos()) {
                    if (a != null) {
                        MimeBodyPart messageBodyPart = new MimeBodyPart();
                        messageBodyPart.attachFile(a);
                        try {
                            String nombre = messageBodyPart.getFileName();
//                            this.identificarDocumentoRespuesta.getIdenticacion();
                            nombre = this.email.getNombreDocumento() + nombre.replaceAll("Out", "");
                            messageBodyPart.setFileName(nombre);
                        } catch (MessagingException e) {
                            Logger.getLogger("global").log(Level.INFO, e.toString());
                        }
                        multipart.addBodyPart(messageBodyPart);
                    }
                }

                Transport transport = session.getTransport("smtp");
                for (String emailDestinario : email.getListEmails()) {
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDestinario));
                }
                multipart.addBodyPart(htmlPart);
                message.setContent(multipart);
                transport.send(message);
                transport.close();
            }
        } catch (MessagingException | UnsupportedEncodingException ex) {
            log.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }


}
