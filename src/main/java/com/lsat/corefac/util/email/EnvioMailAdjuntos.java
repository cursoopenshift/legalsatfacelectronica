package com.lsat.corefac.util.email;

import com.lsat.corefac.util.IdentificarDocumento;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Getter
@Setter
@Slf4j
public class EnvioMailAdjuntos {

    private boolean accesoSSL;
    private boolean instaciaServidorAplicacion;
    private String userName = "";
    private String correo;
    private String clave;
    private String cuerpo;
    private String asunto;
    private boolean debug;

    private Map<String,Object> propiedades;
    private String nombreDcoumento;
    private String xmlContenido;

    public EnvioMailAdjuntos() {
        this.cuerpo = "";
        this.asunto = "";
    }

    public boolean isAccesoSSL() {
        return this.accesoSSL;
    }

    public void setAccesoSSL(boolean accesoSSL) {
        this.accesoSSL = accesoSSL;
    }

    public void setAccesoSSL(String accesoSSL) {
        if (accesoSSL.compareTo("true") == 0) {
            this.accesoSSL = true;
        } else {
            this.accesoSSL = false;
        }
    }


    public void enviarCorreoAdjuntosDocumentosElectronicos(EmailUtil email) {
        cargarFormatos(email);
        email.setAsunto(this.asunto);
        email.setContenido(this.cuerpo);
       // email.setPropiedades(this.propiedades);
        email.setNombreDocumento(this.nombreDcoumento);
        EnvioEmailThread envioThread = new EnvioEmailThread(email);
        envioThread.setUser(getUserName());
        envioThread.setP(clave);

        envioThread.start();

    }

    private void cargarFormatos(EmailUtil emailUtil) {

        this.cuerpo = "Documento Electronico enviado";
        this.asunto = "Documento Electronico enviado";

        IdentificarDocumento   identificarDocumentoRespuesta = new IdentificarDocumento(this.xmlContenido);
        identificarDocumentoRespuesta.descuartizaXMLAutorizado();
        this.asunto = String.valueOf(this.propiedades.get("EMAIL_ASUSNTO"));
        this.asunto = this.asunto.replaceAll("#rucEmpresa", identificarDocumentoRespuesta.getIdenticacion());
        this.asunto = this.asunto.replaceAll("#nombreEmpresa", identificarDocumentoRespuesta.getNombreEmpresa());
        this.asunto = this.asunto.replaceAll("#rucCliente", identificarDocumentoRespuesta.getIdentificacionCliente());
        this.asunto = this.asunto.replaceAll("#nombreCliente", identificarDocumentoRespuesta.getNombreCliente());
        this.asunto = this.asunto.replaceAll("#secuencaDocumento",identificarDocumentoRespuesta.getSecuencia());
        this.asunto = this.asunto.replaceAll("#nombreDocumento", identificarDocumentoRespuesta.getNombreTipoDocumento());
        this.asunto = this.asunto.replaceAll("Nota de Cr�dito", "Nota de Crédito");
        this.asunto = this.asunto.replaceAll("Nota de D�bito", "Nota de Débito");
        this.asunto = this.asunto.replaceAll("Guías de Remisi�n", "Guías de Remisión");

        this.cuerpo = String.valueOf(this.propiedades.get("EMAIL_CUERPO"));
        this.cuerpo = this.cuerpo.replaceAll("#rucEmpresa", identificarDocumentoRespuesta.getIdenticacion());
        this.cuerpo = this.cuerpo.replaceAll("#nombreEmpresa", identificarDocumentoRespuesta.getNombreEmpresa());
        this.cuerpo = this.cuerpo.replaceAll("#rucCliente", identificarDocumentoRespuesta.getIdentificacionCliente());
        this.cuerpo = this.cuerpo.replaceAll("#nombreCliente", identificarDocumentoRespuesta.getNombreCliente());
        this.cuerpo = this.cuerpo.replaceAll("#secuencaDocumento", identificarDocumentoRespuesta.getSecuencia());
        this.cuerpo = this.cuerpo.replaceAll("#nombreDocumento", identificarDocumentoRespuesta.getNombreTipoDocumento());
        this.cuerpo = this.cuerpo.replaceAll("#valorDocumento", identificarDocumentoRespuesta.getValor());

        this.cuerpo = this.cuerpo.replaceAll("�", "&aacute;");
        this.cuerpo = this.cuerpo.replaceAll("�", "&eacute;");
        this.cuerpo = this.cuerpo.replaceAll("�", "&iacute;");
        this.cuerpo = this.cuerpo.replaceAll("�", "&oacute;");
        this.cuerpo = this.cuerpo.replaceAll("�", "&uacute;");
        this.cuerpo = this.cuerpo.replaceAll("�", "&ntilde;");
        this.nombreDcoumento = (identificarDocumentoRespuesta.getIdenticacion().length() > 0 ? identificarDocumentoRespuesta.getIdenticacion() + "-" : "") + identificarDocumentoRespuesta.getCodigoTipoDocumento() + "-";
    }



}
