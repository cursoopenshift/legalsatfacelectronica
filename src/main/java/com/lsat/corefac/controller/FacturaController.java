package com.lsat.corefac.controller;

import com.lsat.corefac.model.util.FacturaResquest;
import com.lsat.corefac.model.util.ResponseGeneric;
import com.lsat.corefac.service.FacturaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

import java.text.ParseException;


@Slf4j
@RestController
@RequestMapping("/api/fac")
public class FacturaController {

    @Autowired
    private FacturaService facturaService;

    @PostMapping("/emitir")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody ResponseEntity<ResponseGeneric> emitir(@RequestBody FacturaResquest facturaResquest) {

        try {
            return ResponseEntity.ok().body(facturaService.emitirFactura(facturaResquest));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }




}
