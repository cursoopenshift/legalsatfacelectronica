package com.lsat.corefac.service;

import com.lsat.corefac.model.Company;
import com.lsat.corefac.model.Document;
import com.lsat.corefac.model.Xml;
import com.lsat.corefac.model.util.FacturaResquest;
import com.lsat.corefac.model.util.ResponseGeneric;
import com.lsat.corefac.model.util.docs.FE.*;
import com.lsat.corefac.repository.CompanyRepository;
import com.lsat.corefac.repository.DocumentRepository;
import com.lsat.corefac.repository.XmlRepository;
import com.lsat.corefac.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.Optional;


@Slf4j
@Service
public class FacturaService {


    private final DocumentRepository documentRepository;
    private final CompanyRepository companyRepository;
    private final XmlRepository xmlRepository;
    @Autowired
    private ProcesadorGeneral procesadorGeneral;

    @Value("${folder.root.pro}")
    String raizPath;

    @Autowired
    public FacturaService(DocumentRepository documentRepository, CompanyRepository companyRepository, XmlRepository xmlRepository) {
        this.documentRepository = documentRepository;
        this.companyRepository = companyRepository;
        this.xmlRepository = xmlRepository;
    }

    public ResponseGeneric emitirFactura(FacturaResquest facturaResquest) throws ParseException {

        Optional<Company> oneCompany = this.companyRepository.findById(facturaResquest.getCcompany());
        if (!oneCompany.isPresent()) {
            log.error("La compania no existe");
            return new ResponseGeneric(true, "msg_empresa_no_existe", null);
        }
        if (oneCompany.get().getState() != 'A') {
            log.error("La compania no esta activa");
            return new ResponseGeneric(true, "msg_empresa_no_existe", null);
        }
        String claveAcceso = ClaveDeAcceso.generaClave(UtilConvert.convertStringToDate(facturaResquest.getEmisor().getFechaEmision(), UtilConvert.DATE_ONLY_FORMAT), facturaResquest.getCodigoDoc(), facturaResquest.getEmisor().getRuc(), facturaResquest.getEmisor().getAmbiente(), (facturaResquest.getEmisor().getEstablecimiento() + facturaResquest.getEmisor().getPtoEmision()), facturaResquest.getEmisor().getSecuencial(), "12345678", facturaResquest.getEmisor().getTipoEmision());
        String numeroDocumento=facturaResquest.getEmisor().getEstablecimiento() +"_"+ facturaResquest.getEmisor().getPtoEmision()+"_"+ facturaResquest.getEmisor().getSecuencial();
        String ruc=facturaResquest.getEmisor().getRuc();
        Factura factura = new Factura();
        factura.setVersion(facturaResquest.getVersion());
        factura.setId("comprobante");
        InfoTributaria infoTributaria = new InfoTributaria();
        infoTributaria.setAmbiente(facturaResquest.getEmisor().getAmbiente());
        infoTributaria.setTipoEmision(facturaResquest.getEmisor().getTipoEmision());
        infoTributaria.setRazonSocial(facturaResquest.getEmisor().getRazonSocial());
        infoTributaria.setNombreComercial(facturaResquest.getEmisor().getNombreComercial());
        infoTributaria.setRuc(facturaResquest.getEmisor().getRuc());
        infoTributaria.setClaveAcceso(claveAcceso);
        infoTributaria.setCodDoc(facturaResquest.getEmisor().getCodDoc());
        infoTributaria.setEstab(facturaResquest.getEmisor().getEstablecimiento());
        infoTributaria.setPtoEmi(facturaResquest.getEmisor().getPtoEmision());
        infoTributaria.setSecuencial(facturaResquest.getEmisor().getSecuencial());
        infoTributaria.setDirMatriz(facturaResquest.getEmisor().getDirEstablecimiento());



        factura.setInfoTributaria(infoTributaria);
        ///////////////////////////////////////////////////////////////////

        Factura.Detalles detalles = new Factura.Detalles();

        double totalDesucuento = 0;
        double totalSinImpuestos = 0;
        for (FacturaResquest.Item item : facturaResquest.getItems()) {
            Factura.Detalles.Detalle oneDetalle = new Factura.Detalles.Detalle();
            oneDetalle.setCodigoPrincipal(item.getCodigo_principal());
            oneDetalle.setUnidadMedida(null);
            oneDetalle.setCantidad(new BigDecimal(item.getCantidad()));
            oneDetalle.setDescripcion(item.getDescripcion());
            oneDetalle.setCodigoAuxiliar(item.getCodigoauxiliar());
            oneDetalle.setPrecioUnitario(item.getPrecioUnitario());
            oneDetalle.setDescuento(item.getDescuento());
            totalSinImpuestos = totalSinImpuestos + (oneDetalle.getCantidad().doubleValue() * item.getPrecioUnitario().doubleValue());
            totalDesucuento = totalDesucuento + item.getDescuento().doubleValue();
            oneDetalle.setPrecioTotalSinImpuesto(new BigDecimal((oneDetalle.getCantidad().doubleValue() * item.getPrecioUnitario().doubleValue()) - oneDetalle.getDescuento().doubleValue()));

            if (item.getImpuestos() != null) {
                oneDetalle.setImpuestos(new Factura.Detalles.Detalle.Impuestos());
                for (FacturaResquest.Item.Impuesto oneImpuesto : item.getImpuestos()) {
                    Impuesto impuestoFac = new Impuesto();
                    impuestoFac.setCodigo(oneImpuesto.getCodigo());
                    impuestoFac.setCodigoPorcentaje(oneImpuesto.getCodigoPorcentaje());
                    impuestoFac.setValor(oneImpuesto.getValor());
                    oneImpuesto.setBaseImponible(oneImpuesto.getBaseImponible());
                    oneDetalle.getImpuestos().getImpuesto().add(impuestoFac);
                }
            }
            detalles.getDetalle().add(oneDetalle);
        }




        Factura.InfoFactura infoFactura = new Factura.InfoFactura();
        infoFactura.setFechaEmision(facturaResquest.getEmisor().getFechaEmision());
        infoFactura.setDirEstablecimiento(facturaResquest.getEmisor().getDirEstablecimiento());
        infoFactura.setContribuyenteEspecial(facturaResquest.getEmisor().getContribuyenteEspecial());
        infoFactura.setObligadoContabilidad(facturaResquest.getEmisor().getObligadoLlevarContabilidad().equalsIgnoreCase("SI") ? ObligadoContabilidad.SI : ObligadoContabilidad.NO);
        infoFactura.setTipoIdentificacionComprador(UtilTabla.Table6(facturaResquest.getComprador().getTipoIdentificacion()));
        infoFactura.setGuiaRemision(null);
        infoFactura.setRazonSocialComprador(facturaResquest.getComprador().getRazonSocial());
        infoFactura.setIdentificacionComprador(facturaResquest.getComprador().getIdentificacion());
        infoFactura.setDireccionComprador(facturaResquest.getComprador().getDireccion());
        infoFactura.setTotalSinImpuestos(new BigDecimal(totalSinImpuestos));
        infoFactura.setTotalDescuento(new BigDecimal(totalDesucuento));
//forma de pago
        if(facturaResquest.getPagos()!=null && !facturaResquest.getPagos().isEmpty()){
            Pagos pago=  new Pagos();
            for (FacturaResquest.Pago p:  facturaResquest.getPagos()) {
                Pagos.Pago pagoF = new Pagos.Pago();
                pagoF.setFormaPago(p.getFormaPago());
                pagoF.setPlazo(pagoF.getPlazo());
                pagoF.setUnidadTiempo(p.getUnidadTiempo());
                pagoF.setTotal(p.getTotal());
                pago.getPago().add(pagoF);
            }
            infoFactura.setPagos(pago);
        }
        //total con impuestos
        infoFactura.setTotalConImpuestos();



        factura.setInfoFactura(infoFactura);
        factura.setDetalles(detalles);
        /////////////////////////////////////////////////////////////////////////////////////////
        String xml = UtilConvert.jaxbObjectToXML(factura, Factura.class);

        if (xml == null) {
            log.error("Error al Generar el Xml");
            return new ResponseGeneric(true, "msg_error_generar_xml", null);
        }
        String pathEmisior=  StringUtils.generatePathLocate(raizPath,ruc);
        String pathDoc=  StringUtils.generateNameFile(pathEmisior,UtilTabla.nombreDocumento(facturaResquest.getCodigoDoc()),facturaResquest.getEmisor().getFechaEmision(),numeroDocumento,claveAcceso);
        Optional<Document> oneDocumento = this.documentRepository.findById(facturaResquest.getCdocument());
        Optional<Xml> oneXml = null;
        if (!oneDocumento.isPresent()) {
            Document document = new Document();
            document.setEmision("1");
            document.setCcompany(oneCompany.get().getCompany());
            document.setAliasDoc(facturaResquest.getCodigoDoc());
            document.setDateRegistre(new Date());
            document.setName(UtilTabla.nombreDocumento(facturaResquest.getCodigoDoc()));
            document.setKeyAcces(claveAcceso);
            document.setAmbiente(facturaResquest.getEmisor().getAmbiente());
            document.setValue(new Double(0));
            document.setDateSRI("");
            document.setState(UtilEnum.EstadoDocumento.REGISTRADO.getCode());
            document.setPaso(UtilEnum.PASO.REGISTRO);
            document.setPathLocal(pathEmisior);
            document.setPathAutorizado(pathDoc.replaceAll(".xml","Out.xml"));
            document.setPathRide(pathDoc.replaceAll(".xml",".pdf"));
            document.setRuc(ruc);
            oneDocumento = Optional.of(this.documentRepository.save(document));
            Xml xml1 = new Xml();
            xml1.setXmlNormal(xml);
            xml1.setCdocument(document.getCdocument());
            oneXml = Optional.of(this.xmlRepository.save(xml1));
        }else{
            oneXml=this.xmlRepository.findByCode(oneDocumento.get().getCdocument());
        }
        try {
            this.procesadorGeneral.execute(oneCompany.get(), oneDocumento.get(), oneXml.get());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return new ResponseGeneric<>(true, "OK", null);

    }




    private String generarDoc() {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> <factura id=\"comprobante\" version=\"1.0.0\"> <infoTributaria> <ambiente>1</ambiente> <tipoEmision>1</tipoEmision> <razonSocial>Jorge Luis</razonSocial> <nombreComercial>Ideas en Binario</nombreComercial> <ruc>0919663062001</ruc> <claveAcceso>0301202301123456789000110010030000000071234567811</claveAcceso> <codDoc>01</codDoc> <estab>001</estab> <ptoEmi>003</ptoEmi> <secuencial>000000007</secuencial> <dirMatriz>Ecuador</dirMatriz> </infoTributaria> <infoFactura> <fechaEmision>03/01/2023</fechaEmision> <dirEstablecimiento>Ecuador</dirEstablecimiento> <obligadoContabilidad>NO</obligadoContabilidad> <tipoIdentificacionComprador>07</tipoIdentificacionComprador> <razonSocialComprador>CONSUMIDOR FINAL</razonSocialComprador> <identificacionComprador>9999999999999</identificacionComprador> <totalSinImpuestos>10.00</totalSinImpuestos> <totalDescuento>0.00</totalDescuento> <totalConImpuestos> <totalImpuesto> <codigo>2</codigo> <codigoPorcentaje>2</codigoPorcentaje> <baseImponible>10.00</baseImponible> <tarifa>12.00</tarifa> <valor>1.20</valor> </totalImpuesto> </totalConImpuestos> <propina>0.00</propina> <importeTotal>11.20</importeTotal> <moneda>DOLAR</moneda> <pagos> <pago> <formaPago>01</formaPago> <total>11.20</total> </pago> </pagos> </infoFactura> <detalles> <detalle> <codigoPrincipal>3</codigoPrincipal> <descripcion>NORMAL CON IVA</descripcion> <cantidad>1</cantidad> <precioUnitario>10</precioUnitario> <descuento>0</descuento> <precioTotalSinImpuesto>10.00</precioTotalSinImpuesto> <impuestos> <impuesto> <codigo>2</codigo> <codigoPorcentaje>2</codigoPorcentaje> <tarifa>12.00</tarifa> <baseImponible>10.00</baseImponible> <valor>1.20</valor> </impuesto> </impuestos> </detalle> </detalles> </factura>";
        return xml;
    }


}
