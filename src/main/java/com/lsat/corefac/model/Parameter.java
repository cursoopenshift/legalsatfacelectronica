package com.lsat.corefac.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name = "tparameter")
@Getter
@Setter
public class Parameter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Long cparameter;
    @Column(name = "ccompany", nullable = false)
    private Long ccompany;

    @Column(name = "code", length = 32)
    protected String code;

    @Column(name = "description", length = 125)
    protected String description;

    @Column(name = "valor1", length = 255)
    protected String valor1;

    @Column(name = "valor2", length = 255)
    protected String valor2;

    @Column(name = "valor3", length = 255)
    protected String valor3;

    @Column(name = "valor4", length = 255)
    protected String valor4;

}
