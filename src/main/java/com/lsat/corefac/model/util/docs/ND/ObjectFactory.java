
package com.lsat.corefac.model.util.docs.ND;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the generated package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated.
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link generatedNotaDebito.NotaDebito}.
     *
     * @return
     */
    public NotaDebito createNotaDebito() {
        return new NotaDebito();
    }

    /**
     * Create an instance of {@link NotaDebito.InfoAdicional}.
     *
     * @return
     */
    public NotaDebito.InfoAdicional createNotaDebitoInfoAdicional() {
        return new NotaDebito.InfoAdicional();
    }

    /**
     * Create an instance of {@link NotaDebito.Motivos}.
     *
     * @return
     */
    public NotaDebito.Motivos createNotaDebitoMotivos() {
        return new NotaDebito.Motivos();
    }

    /**
     * Create an instance of {@link NotaDebito.InfoNotaDebito}.
     *
     * @return
     */
    public NotaDebito.InfoNotaDebito createNotaDebitoInfoNotaDebito() {
        return new NotaDebito.InfoNotaDebito();
    }

    /**
     * Create an instance of {@link generatedNotaDebito.InfoTributaria}.
     *
     * @return
     */
    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    /**
     * Create an instance of {@link generatedNotaDebito.Detalle}.
     *
     * @return
     */
    public Detalle createDetalle() {
        return new Detalle();
    }

    /**
     * Create an instance of {@link generatedNotaDebito.Impuesto}.
     *
     * @return
     */
    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    /**
     * Create an instance of {@link NotaDebito.InfoAdicional.CampoAdicional}.
     *
     * @return
     */
    public NotaDebito.InfoAdicional.CampoAdicional createNotaDebitoInfoAdicionalCampoAdicional() {
        return new NotaDebito.InfoAdicional.CampoAdicional();
    }

    /**
     * Create an instance of {@link NotaDebito.Motivos.Motivo}.
     *
     * @return
     */
    public NotaDebito.Motivos.Motivo createNotaDebitoMotivosMotivo() {
        return new NotaDebito.Motivos.Motivo();
    }

    /**
     * Create an instance of {@link NotaDebito.InfoNotaDebito.Impuestos}.
     *
     * @return
     */
    public NotaDebito.InfoNotaDebito.Impuestos createNotaDebitoInfoNotaDebitoImpuestos() {
        return new NotaDebito.InfoNotaDebito.Impuestos();
    }

}
