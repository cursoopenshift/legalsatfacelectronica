package com.lsat.corefac.model.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class ResponseGeneric<T> {
    private boolean error;
    private String message;
    private Map<String, T> data;



}
