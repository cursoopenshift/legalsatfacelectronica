/*    */ package com.lsat.corefac.model.util.docs.CR;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlRegistry;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlRegistry
/*    */ public class ObjectFactory
/*    */ {
/*    */   public ComprobanteRetencion createComprobanteRetencion()
/*    */   {
/* 38 */     return new ComprobanteRetencion();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ComprobanteRetencion.InfoAdicional createComprobanteRetencionInfoAdicional()
/*    */   {
/* 46 */     return new ComprobanteRetencion.InfoAdicional();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public InfoTributaria createInfoTributaria()
/*    */   {
/* 54 */     return new InfoTributaria();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ComprobanteRetencion.InfoCompRetencion createComprobanteRetencionInfoCompRetencion()
/*    */   {
/* 62 */     return new ComprobanteRetencion.InfoCompRetencion();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ComprobanteRetencion.Impuestos createComprobanteRetencionImpuestos()
/*    */   {
/* 70 */     return new ComprobanteRetencion.Impuestos();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public Impuesto createImpuesto()
/*    */   {
/* 78 */     return new Impuesto();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ComprobanteRetencion.InfoAdicional.CampoAdicional createComprobanteRetencionInfoAdicionalCampoAdicional()
/*    */   {
/* 86 */     return new ComprobanteRetencion.InfoAdicional.CampoAdicional();
/*    */   }
/*    */ }


/* Location:              F:\SRI001.war!\WEB-INF\classes\com\documentosElectronicos\comprobanteRetencion\ObjectFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */