/*     */ package com.lsat.corefac.model.util.docs.ND;
/*     */
/*     */ import java.math.BigDecimal;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name = "impuesto", propOrder = {"codigo", "codigoPorcentaje", "tarifa", "baseImponible", "valor"})
/*     */ public class Impuesto /*     */ {
    /*     */ @XmlElement(required = true)
    /*     */ protected String codigo;
    /*     */    @XmlElement(required = true)
    /*     */ protected String codigoPorcentaje;
    /*     */    @XmlElement(required = true)
    /*     */ protected BigDecimal tarifa;
    /*     */    @XmlElement(required = true)
    /*     */ protected BigDecimal baseImponible;
    /*     */    @XmlElement(required = true)
    /*     */ protected BigDecimal valor;
    /*     */
    /*     */ public String getCodigo() /*     */ {
        /*  63 */ return this.codigo;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public void setCodigo(String value) /*     */ {
        /*  75 */ this.codigo = value;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public String getCodigoPorcentaje() /*     */ {
        /*  87 */ return this.codigoPorcentaje;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public void setCodigoPorcentaje(String value) /*     */ {
        /*  99 */ this.codigoPorcentaje = value;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public BigDecimal getTarifa() /*     */ {
        /* 111 */ return this.tarifa;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public void setTarifa(BigDecimal value) /*     */ {
        /* 123 */ this.tarifa = value;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public BigDecimal getBaseImponible() /*     */ {
        /* 135 */ return this.baseImponible;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public void setBaseImponible(BigDecimal value) /*     */ {
        /* 147 */ this.baseImponible = value;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public BigDecimal getValor() /*     */ {
        /* 159 */ return this.valor;
        /*     */    }
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */
    /*     */ public void setValor(BigDecimal value) /*     */ {
        /* 171 */ this.valor = value;
        /*     */    }
    /*     */ }


/* Location:              F:\SRI001.war!\WEB-INF\classes\com\documentosElectronicos\notaDebito\Impuesto.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */
