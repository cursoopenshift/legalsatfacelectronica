
package com.lsat.corefac.model.util.docs.GR;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the generated package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated.
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link generatedGuiaRemision.GuiaRemision}.
     *
     * @return
     */
    public GuiaRemision createGuiaRemision() {
        return new GuiaRemision();
    }

    /**
     * Create an instance of {@link generatedGuiaRemision.Detalle}.
     *
     * @return
     */
    public Detalle createDetalle() {
        return new Detalle();
    }

    /**
     * Create an instance of {@link Detalle.DetallesAdicionales}.
     *
     * @return
     */
    public Detalle.DetallesAdicionales createDetalleDetallesAdicionales() {
        return new Detalle.DetallesAdicionales();
    }

    /**
     * Create an instance of {@link generatedGuiaRemision.Destinatario}.
     *
     * @return
     */
    public Destinatario createDestinatario() {
        return new Destinatario();
    }

    /**
     * Create an instance of {@link GuiaRemision.InfoAdicional}.
     *
     * @return
     */
    public GuiaRemision.InfoAdicional createGuiaRemisionInfoAdicional() {
        return new GuiaRemision.InfoAdicional();
    }

    /**
     * Create an instance of {@link generatedGuiaRemision.InfoTributaria}.
     *
     * @return
     */
    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    /**
     * Create an instance of {@link GuiaRemision.InfoGuiaRemision}.
     *
     * @return
     */
    public GuiaRemision.InfoGuiaRemision createGuiaRemisionInfoGuiaRemision() {
        return new GuiaRemision.InfoGuiaRemision();
    }

    /**
     * Create an instance of {@link GuiaRemision.Destinatarios}.
     *
     * @return
     */
    public GuiaRemision.Destinatarios createGuiaRemisionDestinatarios() {
        return new GuiaRemision.Destinatarios();
    }

    /**
     * Create an instance of {@link Detalle.DetallesAdicionales.DetAdicional}.
     *
     * @return
     */
    public Detalle.DetallesAdicionales.DetAdicional createDetalleDetallesAdicionalesDetAdicional() {
        return new Detalle.DetallesAdicionales.DetAdicional();
    }

    /**
     * Create an instance of {@link Destinatario.Detalles}.
     *
     * @return
     */
    public Destinatario.Detalles createDestinatarioDetalles() {
        return new Destinatario.Detalles();
    }

    /**
     * Create an instance of {@link GuiaRemision.InfoAdicional.CampoAdicional}.
     *
     * @return
     */
    public GuiaRemision.InfoAdicional.CampoAdicional createGuiaRemisionInfoAdicionalCampoAdicional() {
        return new GuiaRemision.InfoAdicional.CampoAdicional();
    }

}
