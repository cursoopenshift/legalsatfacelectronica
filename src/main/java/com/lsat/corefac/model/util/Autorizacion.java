package com.lsat.corefac.model.util;

import com.lsat.corefac.util.IdentificarDocumento;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class Autorizacion {

    private String estado;
    private String fechaAutorizacion;
    private Date fecha;
    private String ambiente;
    private String comprobante;
    private String numeroAutorizacion;
    private List<MensajeAutorizacion> listaMensajeAutorizacion;

    public Autorizacion() {
        setAmbiente("");
        setComprobante("");
        setEstado("");
        setFechaAutorizacion("2000-07-15T18:00:05.752-05:00");
        setListaMensajeAutorizacion(new ArrayList());
        setNumeroAutorizacion("");
    }
}
