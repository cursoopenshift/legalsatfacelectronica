//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.02.05 a las 01:39:11 AM COT 
//


package com.lsat.corefac.model.util.docs.FE;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Factura }
     * 
     */
    public Factura createFactura() {
        return new Factura();
    }

    /**
     * Create an instance of {@link DetalleImpuestos }
     * 
     */
    public DetalleImpuestos createDetalleImpuestos() {
        return new DetalleImpuestos();
    }

    /**
     * Create an instance of {@link Pagos }
     * 
     */
    public Pagos createPagos() {
        return new Pagos();
    }

    /**
     * Create an instance of {@link Factura.InfoAdicional }
     * 
     */
    public Factura.InfoAdicional createFacturaInfoAdicional() {
        return new Factura.InfoAdicional();
    }

    /**
     * Create an instance of {@link Factura.Retenciones }
     * 
     */
    public Factura.Retenciones createFacturaRetenciones() {
        return new Factura.Retenciones();
    }

    /**
     * Create an instance of {@link Reembolsos }
     * 
     */
    public Reembolsos createReembolsos() {
        return new Reembolsos();
    }

    /**
     * Create an instance of {@link Factura.Detalles }
     * 
     */
    public Factura.Detalles createFacturaDetalles() {
        return new Factura.Detalles();
    }

    /**
     * Create an instance of {@link Factura.Detalles.Detalle }
     * 
     */
    public Factura.Detalles.Detalle createFacturaDetallesDetalle() {
        return new Factura.Detalles.Detalle();
    }

    /**
     * Create an instance of {@link Factura.Detalles.Detalle.DetallesAdicionales }
     * 
     */
    public Factura.Detalles.Detalle.DetallesAdicionales createFacturaDetallesDetalleDetallesAdicionales() {
        return new Factura.Detalles.Detalle.DetallesAdicionales();
    }

    /**
     * Create an instance of {@link Factura.InfoFactura }
     * 
     */
    public Factura.InfoFactura createFacturaInfoFactura() {
        return new Factura.InfoFactura();
    }

    /**
     * Create an instance of {@link Factura.InfoFactura.TotalConImpuestos }
     * 
     */
    public Factura.InfoFactura.TotalConImpuestos createFacturaInfoFacturaTotalConImpuestos() {
        return new Factura.InfoFactura.TotalConImpuestos();
    }

    /**
     * Create an instance of {@link InfoTributaria }
     * 
     */
    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    /**
     * Create an instance of {@link TipoNegociable }
     * 
     */
    public TipoNegociable createTipoNegociable() {
        return new TipoNegociable();
    }

    /**
     * Create an instance of {@link Impuesto }
     * 
     */
    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    /**
     * Create an instance of {@link Compensacion }
     * 
     */
    public Compensacion createCompensacion() {
        return new Compensacion();
    }

    /**
     * Create an instance of {@link Compensaciones }
     * 
     */
    public Compensaciones createCompensaciones() {
        return new Compensaciones();
    }

    /**
     * Create an instance of {@link CompensacionesReembolso }
     * 
     */
    public CompensacionesReembolso createCompensacionesReembolso() {
        return new CompensacionesReembolso();
    }

    /**
     * Create an instance of {@link DetalleImpuestos.DetalleImpuesto }
     * 
     */
    public DetalleImpuestos.DetalleImpuesto createDetalleImpuestosDetalleImpuesto() {
        return new DetalleImpuestos.DetalleImpuesto();
    }

    /**
     * Create an instance of {@link Pagos.Pago }
     * 
     */
    public Pagos.Pago createPagosPago() {
        return new Pagos.Pago();
    }

    /**
     * Create an instance of {@link Factura.InfoAdicional.CampoAdicional }
     * 
     */
    public Factura.InfoAdicional.CampoAdicional createFacturaInfoAdicionalCampoAdicional() {
        return new Factura.InfoAdicional.CampoAdicional();
    }

    /**
     * Create an instance of {@link Factura.Retenciones.Retencion }
     * 
     */
    public Factura.Retenciones.Retencion createFacturaRetencionesRetencion() {
        return new Factura.Retenciones.Retencion();
    }

    /**
     * Create an instance of {@link Reembolsos.ReembolsoDetalle }
     * 
     */
    public Reembolsos.ReembolsoDetalle createReembolsosReembolsoDetalle() {
        return new Reembolsos.ReembolsoDetalle();
    }

    /**
     * Create an instance of {@link Factura.Detalles.Detalle.Impuestos }
     * 
     */
    public Factura.Detalles.Detalle.Impuestos createFacturaDetallesDetalleImpuestos() {
        return new Factura.Detalles.Detalle.Impuestos();
    }

    /**
     * Create an instance of {@link Factura.Detalles.Detalle.DetallesAdicionales.DetAdicional }
     * 
     */
    public Factura.Detalles.Detalle.DetallesAdicionales.DetAdicional createFacturaDetallesDetalleDetallesAdicionalesDetAdicional() {
        return new Factura.Detalles.Detalle.DetallesAdicionales.DetAdicional();
    }

    /**
     * Create an instance of {@link Factura.InfoFactura.TotalConImpuestos.TotalImpuesto }
     * 
     */
    public Factura.InfoFactura.TotalConImpuestos.TotalImpuesto createFacturaInfoFacturaTotalConImpuestosTotalImpuesto() {
        return new Factura.InfoFactura.TotalConImpuestos.TotalImpuesto();
    }

}
