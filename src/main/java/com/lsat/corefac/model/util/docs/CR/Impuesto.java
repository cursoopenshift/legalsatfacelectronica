/*     */ package com.lsat.corefac.model.util.docs.CR;
/*     */ 
/*     */ import java.math.BigDecimal;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="impuesto", propOrder={"codigo", "codigoRetencion", "baseImponible", "porcentajeRetener", "valorRetenido", "codDocSustento", "numDocSustento", "fechaEmisionDocSustento"})
/*     */ public class Impuesto
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String codigo;
/*     */   @XmlElement(required=true)
/*     */   protected String codigoRetencion;
/*     */   @XmlElement(required=true)
/*     */   protected BigDecimal baseImponible;
/*     */   @XmlElement(required=true)
/*     */   protected BigDecimal porcentajeRetener;
/*     */   @XmlElement(required=true)
/*     */   protected BigDecimal valorRetenido;
/*     */   @XmlElement(required=true)
/*     */   protected String codDocSustento;
/*     */   protected String numDocSustento;
/*     */   protected String fechaEmisionDocSustento;
/*     */   
/*     */   public String getCodigo()
/*     */   {
/*  71 */     return this.codigo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodigo(String value)
/*     */   {
/*  83 */     this.codigo = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCodigoRetencion()
/*     */   {
/*  95 */     return this.codigoRetencion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodigoRetencion(String value)
/*     */   {
/* 107 */     this.codigoRetencion = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigDecimal getBaseImponible()
/*     */   {
/* 119 */     return this.baseImponible;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBaseImponible(BigDecimal value)
/*     */   {
/* 131 */     this.baseImponible = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigDecimal getPorcentajeRetener()
/*     */   {
/* 143 */     return this.porcentajeRetener;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPorcentajeRetener(BigDecimal value)
/*     */   {
/* 155 */     this.porcentajeRetener = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigDecimal getValorRetenido()
/*     */   {
/* 167 */     return this.valorRetenido;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setValorRetenido(BigDecimal value)
/*     */   {
/* 179 */     this.valorRetenido = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCodDocSustento()
/*     */   {
/* 191 */     return this.codDocSustento;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCodDocSustento(String value)
/*     */   {
/* 203 */     this.codDocSustento = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumDocSustento()
/*     */   {
/* 215 */     return this.numDocSustento;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumDocSustento(String value)
/*     */   {
/* 227 */     this.numDocSustento = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFechaEmisionDocSustento()
/*     */   {
/* 239 */     return this.fechaEmisionDocSustento;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFechaEmisionDocSustento(String value)
/*     */   {
/* 251 */     this.fechaEmisionDocSustento = value;
/*     */   }
/*     */ }


/* Location:              F:\SRI001.war!\WEB-INF\classes\com\documentosElectronicos\comprobanteRetencion\Impuesto.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */