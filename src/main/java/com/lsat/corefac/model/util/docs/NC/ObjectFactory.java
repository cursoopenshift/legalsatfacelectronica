
package com.lsat.corefac.model.util.docs.NC;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the generated package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated.
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NotaCredito}.
     *
     * @return
     */
    public NotaCredito createNotaCredito() {
        return new NotaCredito();
    }

    /**
     * Create an instance of {@link TotalConImpuestos}.
     *
     * @return
     */
    public TotalConImpuestos createTotalConImpuestos() {
        return new TotalConImpuestos();
    }

    /**
     * Create an instance of {@link NotaCredito.InfoAdicional}.
     *
     * @return
     */
    public NotaCredito.InfoAdicional createNotaCreditoInfoAdicional() {
        return new NotaCredito.InfoAdicional();
    }

    /**
     * Create an instance of {@link NotaCredito.Detalles}.
     *
     * @return
     */
    public NotaCredito.Detalles createNotaCreditoDetalles() {
        return new NotaCredito.Detalles();
    }

    /**
     * Create an instance of {@link NotaCredito.Detalles.Detalle}.
     *
     * @return
     */
    public NotaCredito.Detalles.Detalle createNotaCreditoDetallesDetalle() {
        return new NotaCredito.Detalles.Detalle();
    }

    /**
     * Create an instance of {@link NotaCredito.Detalles.Detalle.DetallesAdicionales}.
     *
     * @return
     */
    public NotaCredito.Detalles.Detalle.DetallesAdicionales createNotaCreditoDetallesDetalleDetallesAdicionales() {
        return new NotaCredito.Detalles.Detalle.DetallesAdicionales();
    }

    /**
     * Create an instance of {@link generatedNotaCredito.InfoTributaria}.
     *
     * @return
     */
    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    /**
     * Create an instance of {@link NotaCredito.InfoNotaCredito}.
     *
     * @return
     */
    public NotaCredito.InfoNotaCredito createNotaCreditoInfoNotaCredito() {
        return new NotaCredito.InfoNotaCredito();
    }

    /**
     * Create an instance of {@link TotalConImpuestos.TotalImpuesto}.
     *
     * @return
     */
    public TotalConImpuestos.TotalImpuesto createTotalConImpuestosTotalImpuesto() {
        return new TotalConImpuestos.TotalImpuesto();
    }

    /**
     * Create an instance of {@link generatedNotaCredito.Detalle}.
     *
     * @return
     */
    public Detalle createDetalle() {
        return new Detalle();
    }

    /**
     * Create an instance of {@link Impuesto}.
     *
     * @return
     */
    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    /**
     * Create an instance of {@link NotaCredito.InfoAdicional.CampoAdicional}.
     *
     * @return
     */
    public NotaCredito.InfoAdicional.CampoAdicional createNotaCreditoInfoAdicionalCampoAdicional() {
        return new NotaCredito.InfoAdicional.CampoAdicional();
    }

    /**
     * Create an instance of {@link NotaCredito.Detalles.Detalle.Impuestos}.
     *
     * @return
     */
    public NotaCredito.Detalles.Detalle.Impuestos createNotaCreditoDetallesDetalleImpuestos() {
        return new NotaCredito.Detalles.Detalle.Impuestos();
    }

    /**
     * Create an instance of {@link NotaCredito.Detalles.Detalle.DetallesAdicionales.DetAdicional}.
     *
     * @return
     */
    public NotaCredito.Detalles.Detalle.DetallesAdicionales.DetAdicional createNotaCreditoDetallesDetalleDetallesAdicionalesDetAdicional() {
        return new NotaCredito.Detalles.Detalle.DetallesAdicionales.DetAdicional();
    }

}
