package com.lsat.corefac.model.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MensajeAutorizacion {
    private String identificador;
    private String mensaje;
    private String tipo;
    private String informacionAdicional;


    public MensajeAutorizacion() {
        setIdentificador("");
        setMensaje("");
        setTipo("");
        setInformacionAdicional("");
    }


}


