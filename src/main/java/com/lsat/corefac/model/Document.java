package com.lsat.corefac.model;

import javax.persistence.*;

import com.lsat.corefac.util.UtilEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;


@Entity
@Table(name = "tdocument")
@Getter
@Setter
public class Document implements  Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long cdocument;

    @Column(name = "ccompany", nullable = false)
    private Long ccompany;


    @Column(name="name",length = 120)
    private String name;

    @Column(name="keyAcces",length = 120)
    private String keyAcces;
    @Column(name="alias_doc",length = 10, nullable = false)
    private String aliasDoc;

    @Column(name="ambiente",length = 2, nullable = false)
    private String ambiente;

    @Column(name="emision",length = 2, nullable = false)
    private String emision;

    @Column(name="date_sri",length = 50)
    private String dateSRI;
    @Column(name="ruc",length = 50)
    private String ruc;
    @Column(name="num_autorizacion",length = 120)
    private String numAutorizacion;

    @Column(name="state",length = 5, nullable = false)
    private String state;

    @Column(name="value", nullable = false)
    private Double value;

    @Temporal(TIMESTAMP)
    private Date dateRegistre;

    @Temporal(TIMESTAMP)
    private Date dateRecepcion;

    @Temporal(TIMESTAMP)
    private Date dateFinalize;

    @Enumerated(EnumType.ORDINAL)
    private UtilEnum.PASO paso;

    @Column(name="envio_correo")
    private Boolean envioCorreo;

    @Column(name="genera_pdf")
    private Boolean generaPdf;

    @Column(name="estado_autorizacion",length = 30, nullable = true)
    private String estadoAutorizacion;

    @Column(name="path_autorizado",length = 500, nullable = true)
    private String pathAutorizado;

    @Column(name="path_ride",length = 500, nullable = true)
    private String pathRide;

    @Column(name="path_local",length = 300, nullable = true)
    private String pathLocal;

    @Column(name="emails",length = 500, nullable = true)
    private String emails;

    @Transient
    private Boolean error = false;


    @Transient
    private String message;






}
