package com.lsat.corefac.repository;

import com.lsat.corefac.model.Company;
import com.lsat.corefac.model.Document;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface CompanyRepository extends PagingAndSortingRepository<Company, Long> {


}
