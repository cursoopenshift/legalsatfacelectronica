package com.lsat.corefac.repository;

import com.lsat.corefac.model.Document;
import com.lsat.corefac.model.Parameter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParameterRepository extends PagingAndSortingRepository<Parameter, Long> {

    static String SQL_SEARCH_PARAMETER = "select * from tparameter where ccompany=:ccompany and code=:code";
    static String SQL_SEARCH_PARAMETER_COMPANY = "select * from tparameter where ccompany=:ccompany";

    @Query(nativeQuery = true, value = SQL_SEARCH_PARAMETER)
    Optional<Parameter> findByCode(@Param("ccompany") Long ccompany, @Param("code") String code);

    @Query(nativeQuery = true, value = SQL_SEARCH_PARAMETER_COMPANY)
    List<Parameter> findByCcompany(@Param("ccompany") Long ccompany);

}
