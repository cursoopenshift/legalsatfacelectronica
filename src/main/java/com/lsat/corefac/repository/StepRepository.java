package com.lsat.corefac.repository;

import com.lsat.corefac.model.Step;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StepRepository extends PagingAndSortingRepository<Step, Long> {



}
