package com.lsat.corefac.repository;

import com.lsat.corefac.model.Parameter;
import com.lsat.corefac.model.Xml;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface XmlRepository extends PagingAndSortingRepository<Xml,Long> {

    static String SQL_SEARCH_XML = "select * from txml t where t.cdocument =:cdocument";
    @Query(nativeQuery = true, value = SQL_SEARCH_XML)
    Optional<Xml> findByCode(@Param("cdocument") Long ccompany);

}
